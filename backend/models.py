# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.debug=True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://admin:MarchMadnessMayhem@mmm-database.cjxzu15ya3nf.us-east-1.rds.amazonaws.com:3306'
db = SQLAlchemy(app)

class Players(db.Model) :
    __table_args__ = {'schema':'Players_stats'}
    id = db.Column(db.Integer, primary_key = True)
    teamId = db.Column(db.Integer, db.ForeignKey('Teams.teams.id'))
    name = db.Column(db.String(30))
    position = db.Column(db.String(2))
    games = db.Column(db.Integer)
    fieldGoalsPercentage = db.Column(db.Float)
    rebounds = db.Column(db.Integer)
    assists = db.Column(db.Integer)
    steals = db.Column(db.Integer)
    blocks = db.Column(db.Integer)
    turnovers = db.Column(db.Integer)
    personalFouls = db.Column(db.Integer)
    points = db.Column(db.Integer)
    image = db.Column(db.String(200))


class Teams(db.Model) :
    __tablename__ = 'teams'
    __table_args__ = {'schema':'Teams'}
    id = db.Column(db.Integer, primary_key = True)
    key = db.Column(db.String(10))
    school = db.Column(db.String(20))
    name = db.Column(db.String(20))
    currentWins = db.Column(db.Integer)
    currentLosses = db.Column(db.Integer)
    conference = db.Column(db.String(20))
    stadiumName = db.Column(db.String(20))
    cityLocation = db.Column(db.String(30))
    stateLocation = db.Column(db.String(2))
    logo = db.Column(db.String(200))
    players = db.relationship('Players', backref='teams')
    championships = db.relationship('Championships', backref='teams')

class Championships(db.Model) :
    __table_args__ = {'schema':'Championships'}
    id = db.Column((db.String(4)), primary_key = True)
    champion = db.Column(db.String(10))
    championID = db.Column(db.Integer, db.ForeignKey('Teams.teams.id'))
    coach = db.Column(db.String(20))
    score = db.Column(db.String(10))
    runnerup = db.Column(db.String(20))
    location = db.Column(db.String(30))
    replayUrl = db.Column(db.String(200))



