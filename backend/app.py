# Code citation: Team Geo Players at https://gitlab.com/sarthaksirotiya/cs373-idb

from flask import jsonify, request, Response
from models import app, db, Players, Teams, Championships
from schema import players_schema, teams_schema, championships_schema
from sqlalchemy.sql import text, column, desc
from sqlalchemy import or_
import json

DEFAULT_PAGE_SIZE = 20

@app.route("/")
def home():
    try:
        db.session.query(column('1')).from_statement(text('SELECT 1')).all()
        return '<h1>MMM API</h1>'
    except Exception as e:
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

@app.route("/search/<string:query>")
def search_all(query):
    terms = query.split()
    occurrences = {
        **search_teams(terms),
        **search_championships(terms),
        **search_players(terms),
    }
    objs = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
    teams = [team for team in objs if type(team) == Teams]
    championships = [championship for championship in objs if type(championship) == Championships]
    players = [player for player in objs if type(player) == Players]
    team_results = teams_schema.dump(teams, many=True)
    championship_results = championships_schema.dump(championships, many=True)
    player_results = players_schema.dump(players, many=True)
    return jsonify(
        {"teams": team_results, "championships": championship_results, "players": player_results}
    )


@app.route("/search/<string:model>/<string:query>")
def search_models(model, query):
    model = model.strip().lower()
    terms = query.split()
    result = None
    if model == "team":
        occurrences = search_teams(terms)
        teams = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        result = teams_schema.dump(teams, many=True)
    elif model == "championship":
        occurrences = search_championships(terms)
        championships = sorted(
            occurrences.keys(), key=lambda x: occurrences[x], reverse=True
        )
        result = championships_schema.dump(championships, many=True)
    elif model == "player":
        occurrences = search_players(terms)
        players = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        result = players_schema.dump(players, many=True)
    else:
        return_error(f"Invalid model: {model}")
    return jsonify({"data": result})


def search_teams(terms):
    occurrences = {}
    for term in terms:
        queries = []
        queries.append(Teams.name.contains(term))
        queries.append(Teams.cityLocation.contains(term))
        queries.append(Teams.conference.contains(term))
        queries.append(Teams.key.contains(term))
        queries.append(Teams.school.contains(term))
        queries.append(Teams.stateLocation.contains(term))
        queries.append(Teams.stadiumName.contains(term))
        teams = Teams.query.filter(or_(*queries))
        for team in teams:
            if not team in occurrences:
                occurrences[team] = 1
            else:
                occurrences[team] += 1
    return occurrences


"""
Returns the championships corresponding to the given terms
"""


def search_championships(terms):
    occurrences = {}
    for term in terms:
        queries = []
        team_ids = (
            Teams.query.with_entities(Teams.id).filter(Teams.name.contains(term)).all()
        )
        team_ids = [id[0] for id in team_ids]
        queries.append(Championships.championID.in_(team_ids))
        queries.append(Championships.champion.contains(term))
        queries.append(Championships.id.contains(term))
        queries.append(Championships.location.contains(term))
        queries.append(Championships.runnerup.contains(term))
        championships = Championships.query.filter(or_(*queries))
        for championship in championships:
            if not championship in occurrences:
                occurrences[championship] = 1
            else:
                occurrences[championship] += 1
    return occurrences


"""
Returns the players corresponding to the given terms
"""


def search_players(terms):
    occurrences = {}
    for term in terms:
        queries = []
        team_ids = (
            Teams.query.with_entities(Teams.id).filter(Teams.name.contains(term)).all()
        )
        team_ids = [id[0] for id in team_ids]
        queries.append(Players.teamId.in_(team_ids))
        queries.append(Players.name.contains(term))
        queries.append(Players.position.contains(term))
        queries.append(Players.assists.contains(term))
        queries.append(Players.blocks.contains(term))
        queries.append(Players.personalFouls.contains(term))
        queries.append(Players.points.contains(term))
        queries.append(Players.rebounds.contains(term))
        queries.append(Players.steals.contains(term))
        queries.append(Players.turnovers.contains(term))
        queries.append(Players.fieldGoalsPercentage.contains(term))
        players = Players.query.filter(or_(*queries))
        for player in players:
            if not player in occurrences:
                occurrences[player] = 1
            else:
                occurrences[player] += 1
    return occurrences


@app.route("/players")
def get_players():
    # get args
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    position = request.args.get("position")
    fouls = request.args.get("fouls")
    turnovers = request.args.get("turnovers")
    points = request.args.get("points")
    rebounds = request.args.get("rebounds")
    steals = request.args.get("steals")
    assists = request.args.get("assists")
    sort = request.args.get("sort")
    asc = request.args.get("asc")

    #query
    query = db.session.query(Players)

    # filter
    if position is not None:
            query = query.filter(Players.position == position)

    if fouls is not None:
        fouls_range = fouls.split("-")
        try:
            query = query.filter(
                Players.fouls >= fouls_range[0], Players.fouls <= fouls_range[1]
            )
        except Exception:
            pass

    if turnovers is not None:
        turnovers_range = turnovers.split("-")
        try:
            query = query.filter(
                Players.turnovers >= turnovers_range[0], Players.turnovers <= turnovers_range[1]
            )
        except Exception:
            pass
    if points is not None:
        points_range = points.split("-")
        try:
            query = query.filter(
                Players.points >= points_range[0], Players.points <= points_range[1]
            )
        except Exception:
            pass
    if rebounds is not None:
        rebounds_range = rebounds.split("-")
        try:
            query = query.filter(
                Players.rebounds >= rebounds_range[0], Players.rebounds <= rebounds_range[1]
            )
        except Exception:
            pass
    if steals is not None:
        steals_range = steals.split("-")
        try:
            query = query.filter(
                Players.steals >= steals_range[0], Players.steals <= steals_range[1]
            )
        except Exception:
            pass
    if assists is not None:
        assists_range = assists.split("-")
        try:
            query = query.filter(
                Players.assists >= assists_range[0], Players.assists <= assists_range[1]
            )
        except Exception:
            pass

    # Sort
    if sort is not None and getattr(Teams, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Teams, sort))
        else:
            query = query.order_by(desc(getattr(Teams, sort)))
    count = query.count()
    if (page is not None):
        query = paginate(query, page, perPage)
    
    result = players_schema.dump(query, many=True)
    return jsonify(
        {
            "data": result,
            "meta": {
                "count": count
            }
        }
    )

@app.route("/teams")
def get_teams():
    # get args
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    wins = request.args.get("wins")
    losses = request.args.get("losses")
    sort = request.args.get("sort")
    asc = request.args.get("asc")
    #query
    query = db.session.query(Teams)

    # filter
    if wins is not None:
        wins_range = wins.split("-")
        try:
            query = query.filter(
                Teams.currentWins >= wins_range[0], Teams.currentWins <= wins_range[1]
            )
        except Exception:
            pass
    if losses is not None:
        losses_range = losses.split("-")
        try:
            query = query.filter(
                Teams.currentLosses >= losses_range[0], Teams.currentLosses <= losses_range[1]
            )
        except Exception:
            pass

    # Sort
    if sort is not None and getattr(Teams, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Teams, sort))
        else:
            query = query.order_by(desc(getattr(Teams, sort)))

    count = query.count()
    # paginate query if it's specified
    if page is not None:
        query = paginate(query, page, perPage)
    else:
        query = query.all()
    result = teams_schema.dump(query, many=True)
    return jsonify(
        {
            "data": result,
            "meta": {
                "count": count
            }
        }
    )

@app.route("/championships")
def get_championships():
    # get args
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    score = request.args.get("score")
    sort = request.args.get("sort")
    asc = request.args.get("asc")
    
     # query
    query = db.session.query(Championships)

    # Sort
    if sort is not None and getattr(Championships, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(Championships, sort))
        else:
            query = query.order_by(desc(getattr(Championships, sort)))

    count = query.count()
    # paginate query if it's specified
    if page is not None:
        query = paginate(query, page, perPage)
    else:
        query = query.all()

    result = championships_schema.dump(query, many=True)
    return jsonify(
        {
            "data": result,
            "meta": {
                "count": count
            }
        }
    )

@app.route("/players/<int:r_id>")
def get_player(r_id):
    query = db.session.query(Players).filter_by(id=r_id)
    try:
        result = players_schema.dump(query, many=True)[0]
    except IndexError:
        return return_error(f"Invalid player ID: {r_id}")
    player = query.first()
    result.update({"teamid":player.teamId})
    team = db.session.query(Teams).filter_by(id=player.teamId).first()

    result.update({"championships":championships_schema.dump(team.championships, many=True)})
    return jsonify({
        "data": result
    })



@app.route("/teams/<int:r_id>")
def get_team(r_id):
    query = db.session.query(Teams).filter_by(id=r_id)
    try:
        result = teams_schema.dump(query, many=True)[0]
    except IndexError:
        return return_error(f"Invalid team ID: {r_id}")
    team = query.first()
    team_championships = championships_schema.dump(team.championships, many=True)
    team_players = players_schema.dump(team.players, many=True)
    result.update({"championships" : team_championships})
    result.update({"players" : team_players})
    return jsonify({
        "data": result
    })
    


@app.route("/championships/<int:r_id>")
def get_championship(r_id):
    query = db.session.query(Championships).filter_by(id=r_id)
    try:
        result = championships_schema.dump(query, many=True)[0]
    except IndexError:
        return return_error(f"Invalid championship ID: {r_id}")
    championship = query.first()
    result.update({"championID":championship.championID})
    team = db.session.query(Teams).filter_by(id=championship.championID).first()
    result.update({"players":players_schema.dump(team.players, many=True)})
    return jsonify({
        "data": result
    })

"""
Returns a 404 error with the given msg
"""
def return_error(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="application/json")
    resp.error_code = 404
    return resp

"""
Returns a paginated query according the page number and number per page
"""
def paginate(query, page_num, num_per_page):
    if num_per_page is None:
        num_per_page = DEFAULT_PAGE_SIZE
    return query.paginate(page=page_num, per_page=num_per_page, error_out=False).items


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)