# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

import json
from models import app, db, Players, Teams, Championships

teamNameToID = {}

def populate_db():
    populate_teams()
    populate_players()
    populate_games()


def populate_players():
        with open("data/players_stats.json") as jsn:
            player_data = json.load(jsn)
            for players in player_data:
                db_row = {
                        "id" : players["PlayerID"],
                        "teamId" : players["TeamID"],
                        "name" : players["Name"],
                        "position" : players["Position"],
                        "games" : players["Games"],
                        "fieldGoalsPercentage" : players["FieldGoalsPercentage"],
                        "rebounds" : players["Rebounds"],
                        "assists" : players["Assists"],
                        "steals" : players["Steals"],
                        "blocks" : players["BlockedShots"],
                        "turnovers" : players["Turnovers"],
                        "personalFouls" : players["PersonalFouls"],
                        "points" : players["Points"],
                        "image" : players["Img"]
                }
                db.session.add(Players(**db_row))
            db.session.commit()

def populate_teams():
    with open("data/teams.json") as jsn:
        team_data = json.load(jsn)
        for team in team_data:
            db_row = {
                "id" : team["TeamID"],
                "key" : team["Key"],
                "school" : team["School"],
                "name" : team["Name"],
                "currentWins" : team["Wins"], 
                "currentLosses" : team["Losses"],
                "conference" : team["Conference"],
                "logo": team["TeamLogoUrl"]
            }
            if(team["Stadium"]):
                db_row["stadiumName"] = team["Stadium"]["Name"]
                db_row["cityLocation"] = team["Stadium"]["City"]
                db_row["stateLocation"] = team["Stadium"]["State"]
            db.session.add(Teams(**db_row))
            teamNameToID[team["School"]] = team["TeamID"]
        db.session.commit()

def populate_games():
    with open("data/championships.json") as jsn:
        game_data = json.load(jsn)
        for game in game_data:
            db_row = {
                "id": game["year"],
                "champion": game["champion"],
                "coach": game["coach"],
                "score": game["score"],
                "runnerup": game["runnerup"],
                "location": game["location"],
            }
            if("replay_url" in game):
                db_row["replayUrl"] = game["replay_url"]
            if(game['champion'] != 'Canceled due to Covid-19'):
                db_row["championID"] = teamNameToID[game["champion"]]
            db.session.add(Championships(**db_row))
        db.session.commit()

if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_db()
    