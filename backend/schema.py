# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

from flask_marshmallow import Marshmallow
from models import Players, Teams, Championships
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

ma = Marshmallow()

class PlayerSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Players

class TeamSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Teams

class ChampionshipSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Championships

players_schema = PlayerSchema()
teams_schema = TeamSchema()
championships_schema = ChampionshipSchema()