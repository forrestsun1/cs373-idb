# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

import app
import unittest


class Tests(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def testGetAllPlayers(self):
        with self.client:
            response = self.client.get("/players")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 7906)
    
    def testGetAllTeams(self):
        with self.client:
            response = self.client.get("/teams")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 1156)
        
    def testGetAllChampions(self):
        with self.client:
            response = self.client.get("/championships")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 84)
        
    def testGetPlayerInstance(self):
        with self.client:
            response = self.client.get("/players/60000075")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(data["name"], "Jalen Reynolds")
            self.assertEqual(data["games"], 27)
    
    def testGetTeamInstance(self):
        with self.client:
            response = self.client.get("/teams/1")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(data["name"], "Mustangs")
            self.assertEqual(data["currentWins"], 10)
    
    def testGetJobInstance(self):
        with self.client:
            response = self.client.get("/championships/2022")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(data["champion"], "Kansas")
            
    def testAllSearch(self):
        with self.client:
            response = self.client.get("/search/texas")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            championships = resp["championships"]
            players = resp["players"]
            teams = resp["teams"]
            self.assertEqual(len(championships), 9)
            self.assertEqual(len(players), 0)
            self.assertEqual(len(teams), 21)

    def testModelSearch(self):
        with self.client:
            response = self.client.get("/search/team/texas")
            self.assertEqual(response.status_code, 200)
            resp = response.json
            data = resp["data"]
            self.assertEqual(len(data), 21)
            self.assertEqual(data[0]["school"].find("Texas") != 1, True)


if __name__ == "__main__":
    unittest.main()
