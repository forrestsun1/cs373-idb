#Code used to pull player images into the player stats data

from serpapi import GoogleSearch
import os, json


gcs_api_key = 'AIzaSyAr0p6j4UhhFe8esFBM6h3p_UfyKM3J4KM'

image_results = []

for query in ["Coffee", "boat", "skyrim", "minecraft"]:
    params = {
        "engine": "google",               # search engine. Google, Bing, Yahoo, Naver, Baidu...
        "q": query,                       # search query
        "tbm": "isch",                    # image results
        "num": "100",                     # number of images per page
        "ijn": 0,                         # page number: 0 -> first page, 1 -> second...
        "api_key": gcs_api_key   # your serpapi api key
    }

    search = GoogleSearch(params)         # where data extraction happens

    images_is_present = True
    while images_is_present:
        results = search.get_dict()       # JSON -> Python dictionary

        # checks for "Google hasn't returned any results for this query."
        if "error" not in results:
            for image in results["images_results"]:
                if image["original"] not in image_results:
                    print(image["original"])
                    image_results.append(image["original"])
            
            # update to the next page
            params["ijn"] += 1
        else:
            images_is_present = False
            print(results["error"])

print(json.dumps(image_results, indent=2))
print(len(image_results))




# import requests
# import json

# S = requests.Session()

# URL = "https://en.wikipedia.org/w/api.php"
# PARAMS = {
#     "action": "query",
#     "format": "json",
# #    "titles": "Albert Einstein",
#     "prop": "pageimages",
#     "piprop": "original"
# }

# with open("players_stats_copy.json", "r+") as jsn:
#             player_data = json.load(jsn)
#             for players in player_data:
#                     PARAMS["titles"] = players["Name"]
#                     R = S.get(url=URL, params=PARAMS)
#                     DATA = R.json()
#                     PAGES = DATA['query']['pages']
#                     tgt = PAGES[next(iter(PAGES))]
#                     if "original" in tgt:
#                         players["Img"] = tgt["original"]["source"]
#                     else:
#                         players["Img"] = "NONE"
#             json.dump(player_data, jsn)