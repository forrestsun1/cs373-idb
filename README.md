# March Madness Mayhem

## Group Members

| Name                | EID     |  Gitlab ID     |
| ------------------- | ------- | -------------- |
| Gerardo Rafael Bote | gtb475  | @gbote         |
| Anya Robinson       | amr7993 | @anya.robinson |
| Forrest Sun         | fs8726  | @forrestsun1   |
| Irfan Thomson       | iyt68   | @IrfanThomson  |
| Kenneth Yi          | ky5354  | @kennethyi     |

## Git SHA
**Phase 1:** 2c52604b <br />
**Phase 2:** 7b0d72ae <br />
**Phase 3:** ccf1e4b2 <br />
**Phase 4:** 448689b9 <br />

## Project Leader
**Phase 1:**  Gerardo Rafael Bote @gbote <br />
**Phase 2:**  Forrest Sun @forrestsun1 <br />
**Phase 3:**  Anya Robinson @anya.robinson <br />
**Phase 4:**  Kenneth Yi @kennethyi <br />

## Links
**GitLab Pipelines:** https://gitlab.com/forrestsun1/cs373-idb/-/pipelines <br />
**Website Link:** https://www.marchmadnessmayhem.me/ <br />
**API Documentation:** https://documenter.getpostman.com/view/25819027/2s93CEvbNp 

## Completion Times

### Phase 1
| Name                | Estimated Hours |  Actual Hours  |
| ------------------- | --------------- | -------------- |
| Gerardo Rafael Bote | 20              | 21             |
| Anya Robinson       | 10              | 12             |
| Forrest Sun         | 15              | 17             |
| Irfan Thomson       | 10              | 11             |
| Kenneth Yi          | 10              | 11             |

### Phase 2
| Name                | Estimated Hours |  Actual Hours  |
| ------------------- | --------------- | -------------- |
| Gerardo Rafael Bote | 25              | 22             |
| Anya Robinson       | 18              | 21             |
| Forrest Sun         | 19              | 24             |
| Irfan Thomson       | 20              | 26             |
| Kenneth Yi          | 23              | 26             |

### Phase 3
| Name                | Estimated Hours |  Actual Hours  |
| ------------------- | --------------- | -------------- |
| Gerardo Rafael Bote | 25              | 20             |
| Anya Robinson       | 15              | 10             |
| Forrest Sun         | 10              | 6              |
| Irfan Thomson       | 8               | 10             |
| Kenneth Yi          | 15              | 10             |

### Phase 4
| Name                | Estimated Hours |  Actual Hours  |
| ------------------- | --------------- | -------------- |
| Gerardo Rafael Bote | 7               | 7              |
| Anya Robinson       | 8               | 5              |
| Forrest Sun         | 9               | 6              |
| Irfan Thomson       | 7               | 4              |
| Kenneth Yi          | 7               | 4              |



## Project Information
**Project Name:** March Madness Mayhem

**Project Proposal:** March Madness Mayhem is an informational website displaying the relationship between the teams, the players, and the championship games throughout past March Madness tournaments. For our page listing the championship games since 1939, users can learn which teams have won the most/least championships. Users can also use our Teams page to explore different teams in the NCAA Division I and their various stats throughout the years. For players they’re interested in, users can peruse the Players page to learn more about different players, their backgrounds, statistics, and much more.

## APIs
- March Madness Championship Games (https://rapidapi.com/therundown/api/therundown)
- Teams (https://api.sportsdata.io/v3/cbb/scores/json/teams)
- Players (https://developer.sportradar.com/docs/read/basketball/NCAA_Mens_Basketball_v7)
- About (https://docs.gitlab.com/ee/api/)

## Models

### March Madness Championship Games
- **Estimated Instance Count:** ~80
- **Attributes for Filtering and Sorting:** year, winner, runner-up, winner score, runner-up score, stadium location, MOP
- **Media and Total Attributes:** Twitter feed, stadium location, gallery of pictures from the game
- **Connection to Other Models:** March Madness championship games consist of two teams with many different players that compete for the championship title every year.

### Teams
- **Estimated Instance Count:** ~350
- **Attributes for Filtering and Sorting:** team name, conference, latest season wins, latest season losses, total championships won, home state
- **Media and Total Attributes:** Twitter feed, stadium location, logo, link to team website
- **Connection to Other Models:** Teams have various player rosters and compete with each other to make it to the March Madness championship game.

### Players
- **Estimated Instance Count:** ~1,500
- **Attributes for Filtering and Sorting:** name, position, team, height, seasonal stats (ppg, apg, rpg)
- **Media and Total Attributes:** Twitter feed, player image, player statistics, player accolades
- **Connection to Other Models:** Players are on teams and have the chance to make it into the March Madness bracket and championship game each season.

## Organizational Technique
Currently, we plan to use the same standard organizational technique that was used in past semesters–one page per model with cards for instances.

## Questions
1. Which teams have made the most appearances in March Madness history?
2. What players have contributed the most for their team in March Madness games?
3. What March Madness games were the most surprising? In other words, what games had different outcomes based on their predicted odds?
