# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# docker for front end: runs website on local 3000 port
frontend-docker:
	docker run -dp 3000:3000 mmm-frontend

# Docker for running the backend as a developer on local machine
backend-docker:
	docker run --rm -it -p 5000:5000 mmm-image

# build backend
build-backend:
	docker build -t mmm-image:latest .

#build frontend
build-frontend :
	docker build -t mmm-frontend frontend/

all:

# check each file; check each file's existence with make check
CFILES :=                         \
    .gitlab-ci.yml				  \
	frontend\.gitignore                        

# Check the existence of each check file
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

#run unit tests
python-tests:
	echo "Running python unit tests..."
	python3 backend/tests.py