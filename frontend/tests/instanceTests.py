# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb

import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

URL = "https://www.marchmadnessmayhem.me/"

class Test(unittest.TestCase):

  @classmethod
  def setUpClass(self) -> None:
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    options.experimental_options["prefs"] = chrome_prefs
    # Disable images
    chrome_prefs["profile.default_content_settings"] = {"images": 2}
    self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
    self.driver.get(URL)

  @classmethod
  def tearDownClass(self):
    self.driver.quit()

  def test_games(self):
    self.driver.get(URL + "games")
    self.assertEqual(self.driver.current_url, URL + "games")
  
  def test_teams(self):
    self.driver.get(URL + "teams")
    self.assertEqual(self.driver.current_url, URL + "teams")

  def test_players(self):
    self.driver.get(URL + "players")
    self.assertEqual(self.driver.current_url, URL + "players")

if __name__ == '__main__':
    unittest.main()