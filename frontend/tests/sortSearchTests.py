import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ActionChains

URL = "https://www.marchmadnessmayhem.me/"
postmanURL = "https://documenter.getpostman.com/view/25859804/2s93CEvGRq"

class Test(unittest.TestCase):

    @classmethod
    def setUpClass(self) -> None:
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1440, 900")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        self.driver = webdriver.Chrome(options = options, service = Service(ChromeDriverManager().install()))
        self.driver.get(URL)
        #self.driver.maximize_window()
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    @classmethod
    def ensureCanClick(self, elementCSS):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, elementCSS))
        )
        return WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, elementCSS))
        )
    
    def testTeamConferenceButton(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Teams")))
        element = self.driver.find_elements(By.LINK_TEXT, "Teams")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click conference button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/button")))
        element = self.driver.find_elements(By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/button")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click American Athletic
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]")))
        element = self.driver.find_elements(By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        self.assertEqual(1, 1)

        
    def testPlayersPositionButton(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Players")))
        element = self.driver.find_elements(By.LINK_TEXT, "Players")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Position button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click PG
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        
        self.assertEqual(1, 1)
        
    def testSearch(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/input')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/input')[0]
        
        # Type 'longhorns'
        element.send_keys('longhorns')
        
        newURL = self.driver.current_url
        self.assertIn("search/longhorns", newURL)

   
    

if __name__ == '__main__':
    unittest.main()



