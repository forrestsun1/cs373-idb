import os
from sys import platform

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "./frontend/tests/chromedriver.exe"
    elif platform == "linux":
        PATH = "./frontend/tests/chromedriver_linux"
    else:
        print("Unsupported OS")
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./frontend/tests/splashTests.py")
    os.system("python3 ./frontend/tests/navbarTests.py")
    os.system("python3 ./frontend/tests/instanceTests.py")
    os.system("python3 ./frontend/tests/sortSearchTests.py")
