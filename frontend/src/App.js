import NavBar from "./components/Navbar.js";
import About from "./components/About/About.js";
import Splash from "./components/Splash/Splash.js";
import Games from "./components/Games/Games"
import GameInstance from "./components/Games/GameInstance"
import Players from "./components/Players/Players"
import PlayerInstance from "./components/Players/PlayerInstance"
import Teams from "./components/Teams/Teams"
import TeamInstance from "./components/Teams/TeamInstance"
import Search from './components/Search.js';
import Visualizations from './components/Visualizations/Visualizations.js'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

export default function App() {
  return (
    <div className="App">
      <NavBar />
      <Router>
        <Routes>
          <Route exact path="/" element={<Splash/>} />
          <Route path="/about" element={<About/>} />
          <Route path="/games" element={<Games/>} />
          <Route path="/games/:id" element={<GameInstance/>} />
          <Route path="/players" element={<Players/>} />
          <Route path="/players/:id" element={<PlayerInstance/>} />
          <Route path="/teams" element={<Teams/>} />
          <Route path="/teams/:id" element={<TeamInstance/>} />
          <Route path="/search/:query" element={<Search/>} />
          <Route path="/visualizations" element={<Visualizations/>} />
        </Routes>
      </Router>
    </div>
  );
}
