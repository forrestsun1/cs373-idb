import { render, screen, cleanup } from '@testing-library/react';
import App from "../App";
import GameCard from "../components/Cards/GameCard";
import PlayerCard from "../components/Cards/PlayerCard";
import TeamCard from "../components/Cards/TeamCard";
import Games from "../components/Games/Games";
import Players from "../components/Players/Players";
import Teams from "../components/Teams/Teams";
import Splash from "../components/Splash/Splash";
import About from "../components/About/About";
import Navbar from "../components/Navbar";
import DeveloperCard from '../components/About/Developers';
import { BrowserRouter } from 'react-router-dom';
import FilterDropdown from "../components/FilterDropdown";

describe('Splash pages', () => {
    test('Splash', () => {
        const tree = <BrowserRouter>renderer.create(<Splash />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Players', () => {
        const tree = <BrowserRouter>renderer.create(<Players />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Teams', () => {
        const tree = <BrowserRouter>renderer.create(<Teams />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Games', () => {
        const tree = <BrowserRouter>renderer.create(<Games />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('About', () => {
        const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

})

describe('Page structures', () => {

    test('Player Card', () => {
        const tree = <BrowserRouter>renderer.create(<PlayerCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Team Card', () => {
        const tree = <BrowserRouter>renderer.create(<TeamCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Game Card', () => {
        const tree = <BrowserRouter>renderer.create(<GameCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Developer Card', () => {
        const tree = <BrowserRouter>renderer.create(<DeveloperCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Main App', () => {
        const tree = <BrowserRouter>renderer.create(<App />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });
  
    test('Navigation Bar', () => {
        const tree = <BrowserRouter>renderer.create(<Navbar />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Filter Dropdown', () => {
        const tree = <BrowserRouter>renderer.create(<FilterDropdown />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Filter Dropdown (Initialized)', () => {

        const handleSortFilter = (value) => {
            setSort(value.toLowerCase().replace(" ", "_"))
        }

        const items = [
            "Position",
            "All Positions",
            "Population",
            "PG",
            "SG",
            "SF",
            "PF",
            "C"
        ]

        const props = {
            title: "Sort",
            items: items,
            scroll: null,
            onChange: handleSortFilter,
          };

        const tree = <BrowserRouter>renderer.create(<FilterDropdown props = {props} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });


})


