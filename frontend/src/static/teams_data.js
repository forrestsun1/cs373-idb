export const teams = [
    {
        "id": 0,
        "img": "https://i.bleacherreport.net/images/team_logos/328x328/kansas_jayhawks_football.png?canvas=492,328",
        "School": "Kansas",
        "Name": "Jayhawks",
        "ApRank": 5,
        "Wins": 20,
        "Losses": 5,
        "ConferenceWins": 8,
        "ConferenceLosses": 4,
        "Conference": "Big 12",
        "Stadium": {
            "Name": "Allen Fieldhouse",
            "City": "Lawrence",
            "State": "KS"
        }
    },
    {
        "id": 1,
        "img": "https://upload.wikimedia.org/wikipedia/en/c/cd/Otto_the_orange.png",
        "School": "Syracuse",
        "Name": "Orange",
        "ApRank": "N/A",
        "Wins": 15,
        "Losses": 10,
        "ConferenceWins": 8,
        "ConferenceLosses": 6,
        "Conference": "Atlantic Coast",
        "Stadium": {
            "Name": "JMA Wireless Dome",
            "City": "Syracuse",
            "State": "NY"
        }
    },
    {
        "id": 2,
        "img": "https://i.ebayimg.com/images/g/eLQAAOSw6-Nb0Ogg/s-l400.jpg",
        "School": "North Carolina",
        "Name": "Tar Heels",
        "ApRank": "N/A",
        "Wins": 16,
        "Losses": 10,
        "ConferenceWins": 8,
        "ConferenceLosses": 7,
        "Conference": "Atlantic Coast",
        "Stadium": {
            "Name": "Dean E. Smith Center",
            "City": "Chapel Hill",
            "State": "NC"
        }
    },
    
]