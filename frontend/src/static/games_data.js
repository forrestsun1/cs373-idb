export const games = [
    {
        "id": 0,
        "img": "https://watchstadium.com/wp-content/uploads/2022/04/north-carolina-vs-kansas-2022-ncaa-tournament-championship-game-preview.jpg",
        "year": 2022,
        "winner": "Kansas",
        "runner_up": "North Carolina",
        "winner_score": 72,
        "runner_up_score":  69,
        "location": "Caesars Superdome, New Orleans, Louisiana",
        "mop": "Ochai Agbaji"
    },
    {
        "id": 1,
        "img": "https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagewebpbase64UklGRvwRAgBXRUJQVlA4IPARAgDwxAm-7ec08a035838bd226fb900884b3d9d2c.jpg",
        "year": 2003,
        "winner": "Syracuse",
        "runner_up": "Kansas",
        "winner_score": 81,
        "runner_up_score": 78,
        "location": "Caesars Superdome, New Orleans, Louisiana",
        "mop": "Carmelo Anthony"
    },
    {
        "id": 2,
        "img": "https://i.ytimg.com/vi/-QPB9NBUG2g/maxresdefault.jpg",
        "year": 1993,
        "winner": "North Carolina",
        "runner_up": "Michigan",
        "winner_score": 77,
        "runner_up_score": 71,
        "location": "Caesars Superdome, New Orleans, Louisiana",
        "mop": "Donald Williams"
    }
]