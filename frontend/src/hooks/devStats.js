import axios from "axios";
import { useEffect, useState } from "react";

const useDevStats = () => {
    const [devs, setDevs] = useState();
    const [numCommits, setNumCommits] = useState();
    const [numIssues, setNumIssues] = useState();
    const [loading, setLoading] = useState(false);

    const fetchStats = async () => {
        setLoading(true);

        const devsResponse = await axios.get(
            "https://gitlab.com/api/v4/projects/43384202/users?access_token=glpat-E7JcmaSWszHQj5fqrH7P"
        );
        console.log(devsResponse)
        let commitsResponse = [];
        let fetch = true;
        let page = 1;
        while (fetch) {
            const response = await axios.get(
                `https://gitlab.com/api/v4/projects/43384202/repository/commits?access_token=glpat-E7JcmaSWszHQj5fqrH7P&per_page=100&page=${page}`
            );
            if (response.data.length > 0) {
                commitsResponse = commitsResponse.concat(response.data);
                page++;
            } else {
                fetch = false;
            }
        }
        console.log(commitsResponse)
        let issuesResponse = [];
        fetch = true;
        page = 1;
        while (fetch) {
            const response = await axios.get(
                `https://gitlab.com/api/v4/projects/43384202/issues?access_token=glpat-E7JcmaSWszHQj5fqrH7P&per_page=100&page=${page}&state=closed`
            );
            if (response.data.length > 0) {
                issuesResponse = issuesResponse.concat(response.data);
                page++;
            } else {
                fetch = false;
            }
        }
        const numCommits = commitsResponse.length;
        const numIssues = issuesResponse.length;
        const json = devsResponse.data.map((dev) => ({
            ...dev,
            commits: commitsResponse.filter(
                (commit) =>
                    dev.name
                        .toLowerCase()
                        .includes(commit.committer_name.toLowerCase()) ||
                    dev.username
                        .toLowerCase()
                        .includes(commit.committer_name.toLowerCase())
            ).length,
            issues: issuesResponse.filter(
                (issue) =>
                    issue.closed_by?.name
                        .toLowerCase()
                        .includes(dev.name.toLowerCase()) ||
                    dev.username === issue.closed_by?.username
            ).length,
        }));
        
        console.log(json)
        json.splice(6, 1) // Deals with 2 Gerardo Glitch
        console.log(json)
        setLoading(false);
        setDevs(json);
        setNumCommits(numCommits);
        setNumIssues(numIssues);
    };

    useEffect(() => {
        fetchStats();
    }, []);
    
    return { devs, numCommits, numIssues, loading, refetch: fetchStats };
};

export default useDevStats;
