import React from "react";
import styles from "./Splash.module.css";
import { Container, Row, Col, Card, Button } from 'react-bootstrap';


// Adapted from Finding Footprints: https://gitlab.com/AlejandroCantu/group2

export default function Splash() {
  return (
    <>
    <div className={styles.homePage} />
      <div className={styles.title}>
        March Madness Mayhem
        <div className={styles.subtitle}>
          Your #1 Encyclopedia for March Madness History
        </div>
      </div>
      <Container className="my-4">
          <Row>
              <h2 className="text-center mb-3">March Madness Mayhem Hubs</h2>
              <Col>
                <Card>
                  <Card.Img variant="top" src="https://www.gannett-cdn.com/presto/2022/03/21/USAT/7f8a17dd-fd4c-41cd-b924-c6a471195e1d-March_Madness_file.jpg?crop=4607,2592,x106,y261&width=3200&height=1801&format=pjpg&auto=webp" width="100%"></Card.Img>
                  <Card.Body>
                      <Card.Title>NCAA Men's College Basketball Championship Games</Card.Title>
                      <Card.Text>All NCAA Men's March Madness Championship Games Since 1939.</Card.Text>
                      <Button variant="dark" href="/games">All Championship Games</Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img variant="top" src="https://www.si.com/.image/ar_16:9%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTY4MDI1NzYxNDQ0Mjc1NTg0/college-basketball-353-rankings-update.jpg" width="100%"></Card.Img>
                  <Card.Body>
                      <Card.Title>NCAA Men's College Basketball Teams</Card.Title>
                      <Card.Text>All Current and Past College Men's Basketball Teams.</Card.Text>
                      <Button variant="dark" href="/teams">All NCAA Men's Basketball Teams</Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col>
                <Card>
                  <Card.Img variant="top" src="https://assets1.cbsnewsstatic.com/hub/i/r/2023/03/13/1e1861aa-cf71-4fb5-bd54-a5ad38d054da/thumbnail/1200x630/5b671432ed278815e71f8a2f6d69919b/gettyimages-1248007357.jpg" width="100%"></Card.Img>
                  <Card.Body>
                      <Card.Title>NCAA Men's College Basketball Players</Card.Title>
                      <Card.Text>All Current and Past Men's College Basketball Players.</Card.Text>
                      <Button variant="dark" href="/players">All NCAA Men's Basketball Players</Button>
                  </Card.Body>
                </Card>
              </Col>
          </Row>
      </Container>
    </>
  );
};
