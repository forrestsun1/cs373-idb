import { Spinner } from 'react-bootstrap';

export default function Loading() {
    return (
        <Spinner
            animation="border"
            role="status"
            className="mx-auto"
            style={{ display: "flex", alignSelf: "center" }}
        >
            <span className="sr-only"></span>
        </Spinner>
    );
};