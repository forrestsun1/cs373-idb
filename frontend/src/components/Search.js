import { Container, Row, Col, Nav, Tab } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import axios from 'axios';
import GameCard from '../components/Cards/GameCard';
import TeamCard from '../components/Cards/TeamCard';
import PlayerCard from '../components/Cards/PlayerCard';
import no_player_image from "../assets/no_player_image.png"
import march_madness_court from "../assets/march_madness_court.jpg";
import Loading from "./Loading";
import { useParams } from 'react-router-dom';

export default function Search() {
    const client = axios.create({ baseURL: "https://api.marchmadnessmayhem.me/" });
    const searchQuery = useParams().query;
    const [loaded, setLoaded] = useState(false);
    const [teams, setTeams] = useState([]);
    const [players, setPlayers] = useState([]);
    const [games, setGames] = useState([]);

    const fetch = async () => {
        console.log("Search Query Activated!")
        console.log(searchQuery);
        let queryPlayers = `/search/player/${searchQuery}`;
        console.log(queryPlayers);
        await client
            .get(queryPlayers)
            .then((response) => {
                setPlayers(response["data"].data);
                console.log(response["data"].data);
            });

        let queryTeams = `/search/team/${searchQuery}` ;
        console.log(queryTeams);
        await client
            .get(queryTeams)
            .then((response) => {
                setTeams(response["data"].data);
                console.log(response["data"].data);
            });

        let queryGames = `/search/championship/${searchQuery}`;
        await client
            .get(queryGames)
            .then((response) => {
                setGames(response["data"].data);
                console.log(response["data"].data);
                setLoaded(true);
            });
    }

    useEffect(() => {
        if (!loaded) {
            fetch();
        }
    });

    const highlightText = (text, query) => {
        if (query === "") {
          return text;
        }
      
        const regex = new RegExp(`(${query})`, "gi");
        const parts = text.split(regex);
      
        return parts.map((part, index) =>
          regex.test(part) ? <mark key={index}>{part}</mark> : part
        );
      };

    return (
        <Container>
            <Tab.Container id="left-tabs-example" defaultActiveKey="players">
                <Row>
                    <Col>
                        <Nav variant="tabs" className="justify-content-center">
                            <Nav.Item>
                                <Nav.Link eventKey="players">Players</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="teams">Teams</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="games">Games</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Tab.Content>
                            <Tab.Pane eventKey="players">
                                <Row>
                                    <h3> Search Results for Players </h3>
                                    { loaded ? players.length === 0 ? (<h4>No Players Found.</h4>) : (
                                        players.map((player) => {
                                            return (
                                                <div className="col-sm-4">
                                                    <PlayerCard
                                                        id={player.id}
                                                        name={highlightText(player.name, searchQuery)}
                                                        position={player.position ? highlightText(player.position, searchQuery) : "???"}
                                                        personalFouls={highlightText(player.personalFouls.toString(), searchQuery)}
                                                        points={highlightText(player.points.toString(), searchQuery)}
                                                        rebounds={highlightText(player.rebounds.toString(), searchQuery)}
                                                        steals={highlightText(player.steals.toString(), searchQuery)}
                                                        turnovers={highlightText(player.turnovers.toString(), searchQuery)}
                                                        image={player.image === "NONE" ? no_player_image : player.image}
                                                    />
                                                </div>
                                            );
                                        })
                                    ) : <Loading />
                                }
                                </Row>
                            </Tab.Pane>
                            <Tab.Pane eventKey="teams">
                                <Row>
                                    <h3> Search Results for Teams </h3>
                                    { loaded ? teams.length === 0 ? (<h4>No Teams Found.</h4>) : (
                                        teams.map((team) => {
                                            return (
                                                <div className="col-sm-4">
                                                    <TeamCard
                                                        id={team.id}
                                                        name={highlightText(team.name, searchQuery)}
                                                        school={highlightText(team.school, searchQuery)}
                                                        currentWins={team.currentWins ? highlightText(team.currentWins.toString(), searchQuery) : "???"}
                                                        currentLosses={team.currentLosses ? highlightText(team.currentLosses.toString(), searchQuery) : "???"}
                                                        conference={team.conference ? highlightText(team.conference, searchQuery) : "???"}
                                                        stadiumName={team.stadiumName ? highlightText(team.stadiumName, searchQuery) : "???"}
                                                        cityLocation={team.cityLocation ? highlightText(team.cityLocation, searchQuery) : "???"}
                                                        stadiumLocation={team.stadiumLocation ? highlightText(team.stadiumLocation, searchQuery) : "???"}
                                                        stateLocation={team.stateLocation ? highlightText(team.stateLocation, searchQuery) : "???"}
                                                        logo={team.logo}
                                                    />
                                                </div>
                                            );
                                        })
                                    ) : <Loading />
                                }
                                </Row>
                            </Tab.Pane>
                            <Tab.Pane eventKey="games">
                                <Row>
                                    <h3> Search Results for Games </h3>
                                    { loaded ? games.length === 0 ? (<h4>No Championship Games Found.</h4>) : (
                                        games.map((game) => {
                                            return (
                                                <div className="col-sm-4">
                                                    <GameCard
                                                        id={highlightText(game.id, searchQuery)}
                                                        champion={highlightText(game.champion, searchQuery)}
                                                        runnerup={highlightText(game.runnerup, searchQuery)}
                                                        coach={highlightText(game.coach, searchQuery)}
                                                        score={highlightText(game.score, searchQuery)}
                                                        location={highlightText(game.location, searchQuery)}
                                                        replayUrl={game.replayUrl}
                                                        img={march_madness_court}
                                                    />
                                                </div>
                                            );
                                        })
                                    ) : <Loading />
                                }
                                </Row>
                            </Tab.Pane>
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </Container>
    )
}