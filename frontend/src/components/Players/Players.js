import axios from 'axios';
import React, { useEffect, useState } from "react";
import PlayerCard from "../Cards/PlayerCard";
import no_player_image from "../../assets/no_player_image.png"
import { Container, Row, Col } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import Loading from "../Loading";
import FilterDropdown from "../FilterDropdown";
import Form from "react-bootstrap/Form";
import RangeSlider from '../RangeSlider';

export default function Players() {
  const [players, setPlayers] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [isLoading, setIsLoading] = useState(true); // add state variable
  const [searchQuery, setSearchQuery] = useState("");
  const [positionChoice, setPositionChoice] = useState("All Positions");
  const [personalFouls, setPersonalFouls] = useState([0,100]);
  const [turnovers, setTurnovers] = useState([0,100]);
  const [points, setPoints] = useState([0,500]);
  const [rebounds, setRebounds] = useState([0,200]);
  const [steals, setSteals] = useState([0,100]);
  const [minPersonalFouls, setMinPersonalFouls] = useState(0);
  const [minTurnovers, setMinTurnovers] = useState(0);
  const [minPoints, setMinPoints] = useState(0);
  const [minRebounds, setMinRebounds] = useState(0);
  const [minSteals, setMinSteals] = useState(0);
  const [sortCriteria, setSortCriteria] = useState(""); 
  const [sortOrder, setSortOrder] = useState("");
  const playersPerPage = 21;
  const pagesVisited = pageNumber * playersPerPage;

  useEffect(() => {
    const loadPlayers = async () => {
      setIsLoading(true); // set isLoading to true before making the API call
      axios.get("https://api.marchmadnessmayhem.me/players")
        .then(response => {
          setPlayers(response["data"].data)
          console.log(response["data"].data)
          setIsLoading(false); // set isLoading to false after data is loaded
        })
        .catch(error => {
          console.error(error);
          setIsLoading(false); // set isLoading to false if there's an error
        });
    }
    loadPlayers();
  },[]);

  // display loading component if isLoading is true
  if (isLoading) {
    return <Loading />;
  }

  const filteredPlayers = players.filter((player) => {
    if (positionChoice === "All Positions") {
      return true;
    }
    return player.position === positionChoice;
  }).filter((player) =>
    player.name.toLowerCase().includes(searchQuery.toLowerCase())
  ).filter((player) =>
    (player.personalFouls >= personalFouls[0] && player.personalFouls <= personalFouls[1]) 
  ).filter((player) => 
    (player.turnovers >= turnovers[0] && player.turnovers <= turnovers[1])
  ).filter((player) => 
    (player.points >= points[0] && player.points <= points[1])
  ).filter((player) => 
    (player.rebounds >= rebounds[0] && player.rebounds <= rebounds[1])
  ).filter((player) => 
    (player.steals >= steals[0] && player.steals <= steals[1])
  ).sort((a, b) => {
    if (sortCriteria === "games") {
      if (sortOrder === "asc") {
        return parseInt(a.games) - parseInt(b.games);
      } else {
        return parseInt(b.games) - parseInt(a.games);
      }
    } else if (sortCriteria === "points") {
      if (sortOrder === "asc") {
        return a.points - b.points;
      } else {
        return b.points - a.points;
      }
    } else if (sortCriteria === "personalFouls") {
      if (sortOrder === "asc") {
        return a.personalFouls - b.personalFouls;
      } else {
        return b.personalFouls - a.personalFouls;
      }
    } else if (sortCriteria === "rebounds") {
      if (sortOrder === "asc") {
        return a.rebounds - b.rebounds;
      } else {
        return b.rebounds - a.rebounds;
      }
    } else if (sortCriteria === "turnovers") {
      if (sortOrder === "asc") {
        return a.turnovers - b.turnovers;
      } else {
        return b.turnovers - a.turnovers;
      }
    }
    return 0;
  });


  const highlightText = (text, query) => {
    if (query === "") {
      return text;
    }
  
    const regex = new RegExp(`(${query})`, "gi");
    const parts = text.split(regex);
  
    return parts.map((part, index) =>
      regex.test(part) ? <mark key={index}>{part}</mark> : part
    );
  };

  const displayPlayers = filteredPlayers
    .slice(pagesVisited, pagesVisited + playersPerPage)
    .map((data) => {
      return (
        <div className="col-sm-4">
          <PlayerCard
              id = {data.id}
              name = {highlightText(data.name, searchQuery)}
              position = {data.position}
              personalFouls = {data.personalFouls}
              points = {data.points}
              rebounds = {data.rebounds}
              steals = {data.steals}
              turnovers = {data.turnovers}
              image= {data.image === "NONE" ? no_player_image : data.image}
          />
        </div>
      )
    }
  );

  const pageCount = Math.ceil(filteredPlayers.length / playersPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const handleFoulsFilter = (value) => {
    setPersonalFouls(value)
  }
  const handleTurnoversFilter = (value) => {
    setTurnovers(value)
  }
  const handlePointsFilter = (value) => {
    setPoints(value)
  }
  const handleReboundsFilter = (value) => {
    setRebounds(value)
  }
  const handleStealsFilter = (value) => {
    setSteals(value)
  }

  const handlePositionClick = (value) => {
    setPositionChoice(value);
    setSearchQuery("");
    setPageNumber(0);
  };

return (
  <Container>
    <h1 className="my-4 d-flex justify-content-center">All NCAA Players</h1>
      <Row className="mt-3">
        <Col className='sm-12 mb-3'>
          <input
            type="text"
            placeholder="Search for a player..."
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </Col>
        <Col className='sm-12 mb-3'>
          <FilterDropdown
            title="Position"
            items={["All Positions", "PG", "SG", "SF", "PF", "C"]}
            scroll={false}
            onChange={handlePositionClick}
            filteredPlayers={filteredPlayers}
            searchQuery={searchQuery}
          />
        </Col>
        <Col className='sm-12 mb-3'>  
            <select value={sortCriteria} onChange={(e) => setSortCriteria(e.target.value)}>
              <option value="" disabled selected hidden>Sort stats...</option>
              <option value="games">Games</option>
              <option value="points">Points</option>
              <option value="personalFouls">Personal Fouls</option>
              <option value="rebounds">Rebounds</option>
              <option value="turnovers">Turnovers</option>
            </select>
            <select value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
              <option value="" disabled selected hidden>Sort by...</option>
              <option default value="asc">Ascending</option>
              <option value="desc">Descending</option>
            </select>
          </Col>
      </Row>
      <Row>
        <Col>
        <Form.Label>Fouls</Form.Label>
            <RangeSlider
              min={0}
              max={100}
              onChange={handleFoulsFilter}
              discrete
            />
        </Col>
        <Col>
        <Form.Label>Turnovers</Form.Label>
            <RangeSlider
              min={0}
              max={100}
              onChange={handleTurnoversFilter}
              discrete
            />
        </Col>
        <Col>
        <Form.Label>Points</Form.Label>
            <RangeSlider
              min={0}
              max={500}
              onChange={handlePointsFilter}
              discrete
            />
        </Col>
        <Col>
        <Form.Label>Rebounds</Form.Label>
            <RangeSlider
              min={0}
              max={200}
              onChange={handleReboundsFilter}
              discrete
            />
        </Col>
        <Col>
        <Form.Label>Steals</Form.Label>
            <RangeSlider
              min={0}
              max={100}
              onChange={handleStealsFilter}
              discrete
            />
        </Col>
      </Row>
      <Row>
        {displayPlayers}
      </Row>
      <p style={{textAlign: "center"}}>Showing {pagesVisited + 1}-{Math.min(filteredPlayers.length ,pagesVisited + playersPerPage)} out of {filteredPlayers.length} players <br></br> <span style={{color:"gray", textAlign: "center"}}>Page {pageNumber + 1} of {pageCount}</span></p>
      <ReactPaginate
        previousLabel={"<"}
        nextLabel={">"}
        pageCount={pageCount}
        onPageChange={changePage}
        containerClassName={"pagination justify-content-center"}
        previousLinkClassName={"page-link"}
        nextLinkClassName={"page-link"}
        disabledClassName={"page-item disabled"}
        activeClassName={"page-item active"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
      />
    </Container>
  )
};