import axios from 'axios';
import React, { useEffect, useState } from "react";
import { Container, Row, Col, Figure, Button } from 'react-bootstrap';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import no_player_image from "../../assets/no_player_image.png"
import { Timeline } from 'react-twitter-widgets'



export default function PlayerInstance(){
    const params = useParams()
    const [playerData, setPlayerData] = useState([]);
    const [playerTeam, setPlayerTeam] = useState("");
    let id = Number(params['id'])

    useEffect(() => {
        const loadPlayer = async () => {
          axios.get(`https://api.marchmadnessmayhem.me/players/${id}`)
            .then(response => {
              setPlayerData(response["data"].data)
              console.log(response["data"].data)
              let player = response["data"].data
              axios.get(`https://api.marchmadnessmayhem.me/teams/${player.teamid}`)
                .then(response => {
                setPlayerTeam(response["data"].data.school + " " + response["data"].data.name)
                console.log(response["data"].data.school + " " + response["data"].data.name)
                })
                .catch(error => {
                console.error(error);
                });
            })
            .catch(error => {
              console.error(error);
            });
        }
        loadPlayer();
      });
    
    return (
       <Container style={{justifyContent:'flex'}} className="d-grid">
            <Row style={{marginTop: '25px'}}>
                <Col>
                    <h1 className="my-4 d-flex justify-content-start"> {playerData.name} </h1>
                </Col>
            </Row>
            <Row>
                <Col className="d-flex justify-content-start">
                    <Figure>
                        <Figure.Image
                            width = "250"
                            height = "370"
                            src={playerData.image === "NONE" ? no_player_image : playerData.image}/>
                    </Figure>
                </Col>
                <Col className="d-flex justify-content-start">
                    <Timeline
                        dataSource={{ sourceType: "list", ownerScreenName: "vance", id: "1589775808188538881" }}
                        options={{ width: "600", height: "300" }}
                    />
                </Col>
            </Row>
            <Row>
                <Col style={{textAlign: 'left'}}>
                    <br></br>
                    <p><b>Position: </b>{playerData.position} </p>
                    <p><b>College Team: </b>{playerTeam} </p>
                    <p><b>Number of Points </b>{playerData.points}</p>
                    <p><b>Number of Rebounds </b>{playerData.rebounds} </p>
                    <p><b>Number of Steals </b>{playerData.steals} </p>
                    <p><b>Number of Turnovers </b>{playerData.turnovers} </p>
                    <p><b>Number of Personal Fouls: </b> {playerData.personalFouls} </p>
                </Col>
                <Col style={{textAlign: 'left'}}>
                <h4>Check out {playerData.name}'s team, the {playerTeam}, and their archive of championship wins:</h4>
                {playerData.championships?.length?
                <ol>
                    {playerData.championships?.map((game) => 
                    <li key={game.key}>
                        <Link to={`/games/${game.id}`}>{game.id} National Championship Against {game.runnerup}</Link>
                    </li>
                    )}
                </ol> : 
                <h5><i>The {playerTeam} do not have any championship wins.</i></h5>
                }
                </Col>  
            </Row>
            <Row>
                <Col style={{textAlign: 'left'}}>
                    <br></br>
                    <Link to={`/teams/${playerData.teamid}`}><Button variant="success" size="lg">Learn More About The {playerTeam}</Button></Link>
                    <br></br>
                </Col>  
            </Row>
       </Container>
    )
}
