// Credit: https://gitlab.com/dandom25/electrends/
import PieChart from "./ProviderPieChart";
import React, { useEffect, useState } from "react";

function SongsInGenresChart() {

    const [pieData, setPieData] = useState([]);

  useEffect(() => {
    let api_url = `https://api.lowkeys.me/songs?page=1&count=166`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );

      let body = []
      body = await response.json()

      console.log("results=" + JSON.stringify(body['songs']))
      setPieData(computePieData(body['songs']));
    };
    getData();
  }, []);

    const computePieData = (songData) => {
        let data = [{"name": "rock", "value": 0},
                    {"name": "pop", "value": 0},
                    {"name": "punk", "value": 0},
                    {"name": "country", "value": 0},
                    {"name": "contemporary", "value": 0}];

        const cnts = [0, 0];

        songData.forEach(song => {
            if(song.genres.includes("rock")){
                data[0].value++;
                ++cnts[0];
            } 
            if(song.genres.includes("pop punk")){
                data[1].value++;
                ++cnts[1];
            }
            if(song.genres.includes("neon pop punk")){
                data[2].value++;
                ++cnts[2];
            }
            if(song.genres.includes("country")){
                data[3].value++;
                ++cnts[3];
            }
            if(song.genres.includes("contemporary country")){
                data[4].value++;
                ++cnts[4];
            }
        });
        console.log("data = " + JSON.stringify(data))
        return data;
    }

    return(
        <div>
            <h4>Percent of Top 5 Genres Out of All Songs</h4>
            <PieChart data={pieData}/>
        </div>
    );



}

export default SongsInGenresChart;