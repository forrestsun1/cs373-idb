// Credit: https://gitlab.com/dandom25/electrends/

import BarChart from "./ProviderBarChart";
import React, { useEffect, useState } from "react";

function NumArtistsChart() {

    const [barData, setBarData] = useState([]);

  useEffect(() => {
    let api_url = `https://api.lowkeys.me/artists?page=1&count=100`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );
      console.log("RESPONSE")
      console.log(response)
      console.log(response.text)
      console.log(response.status)
      console.log(JSON.stringify(response))
      let body = []
      body = await response.json()
      console.log("BODY[artists]")
      console.log((body['artists']))
      setBarData(computeBarData(body['artists']));
    };
    getData();
  }, []);

    const computeBarData = (artistData) => {
        let data = [{"name": "Pop Emo", "numArtists": 0, "fill":"#84BCDA"},
                    {"name": "Pop Punk", "numArtists": 0,  "fill":"#4C6085"},
                    {"name": "Neon Pop Punk", "numArtists": 0, "fill":"#0015bc"}];
        
        const cnts = [0, 0, 0];

        artistData.forEach(artist => {
            if(artist.genres.includes("pop emo") || artist.genres.includes("emo")){
                data[0].numArtists++;
                ++cnts[0];
            }
             if(artist.genres.includes("pop punk")){
                data[1].numArtists++;
                ++cnts[1];
            }
             if(artist.genres.includes("neon pop punk")){
                data[2].numArtists++;
                ++cnts[2];
            }
        });


        console.log(data);
        return data;
    }

    return(
        <div>
            <h4>Number of Artists in Pop/Punk Genre Variations</h4>
            <BarChart data={barData}/>
           
        </div>
    );

}

export default NumArtistsChart;