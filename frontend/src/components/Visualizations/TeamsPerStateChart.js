// Credit: https://gitlab.com/dandom25/electrends/

import BarChart from "./OurBarChart";
import React, { useEffect, useState } from "react";

function TeamsPerStateChart() {

  const [barData, setBarData] = useState([]);

  useEffect(() => {
    var api_url = `https://api.marchmadnessmayhem.me/teams`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );
      console.log("RESPONSE")
      console.log(response)
      console.log(response.text)
      console.log(response.status)
      console.log(JSON.stringify(response))
      let body = []
      body = await response.json()
      console.log("BODY[data]")
      console.log((body['data']))
      setBarData(computeBarData(body['data']));
    };
    getData();
  }, []);

  const computeBarData = (teamData) => {
    var data = [{"name": "AL", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "AK", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "AZ", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "AR", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "CA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "CO", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "CT", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "DE", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "FL", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "GA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "HI", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "ID", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "IL", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "IN", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "IA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "KS", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "KY", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "LA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "ME", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MD", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MI", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MN", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MS", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MO", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "MT", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NE", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NV", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NH", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NJ", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NM", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NY", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "NC", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "ND", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "OH", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "OK", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "OR", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "PA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "RI", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "SC", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "SD", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "TN", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "TX", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "UT", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "VT", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "VA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "WA", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "WV", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "WI", "numTeams": 0, "fill": "#84BCDA"},
                {"name": "WY", "numTeams": 0, "fill": "#84BCDA"},];
        
        const cnts = new Array(50).fill(0);

        teamData.forEach(team => {
          if (team.stateLocation != null) {
            for (let i = 0; i < 50; ++i) {
              if (team.stateLocation.includes(data[i]['name'])) {
                data[i].numTeams++;
                ++cnts[i];
              }
            }
          }
        });

        console.log(data);
        return data;
    }

    return(
        <div>
            <h4>Number of Teams per U.S. state</h4>
            <BarChart data={barData}/>
           
        </div>
    );

}

export default TeamsPerStateChart;