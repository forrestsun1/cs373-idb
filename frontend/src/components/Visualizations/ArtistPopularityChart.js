// Credit: https://gitlab.com/dandom25/electrends/

import ScatterPlot from "./ProviderScatterplot";
import React, { useEffect, useState } from "react";

function ArtistPopularityChart() {

    const [scatterData, setScatterData] = useState([]);

  useEffect(() => {
    let api_url = `https://api.lowkeys.me/artists?page=1&count=100`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );
      let body = []
      body = await response.json()
      console.log("BODY[artists]")
      console.log((body['artists']))
      setScatterData(computeScatterData(body['artists']));
    };
    getData();
  }, []);

    const computeScatterData = (artistData) => {
        const array = [];
  
        
        artistData.forEach(artist => {
           
          array.push({"x": parseInt(artist.popularity), "y": artist.followers});
          
            
        });
        array.sort((a, b) => (a.x > b.x) ? 1 : -1);
      

        
        return [array];
    }

    return(
        <div>
            <h4>Relationship between Artist Popularity and Artist Followers</h4>
            <ScatterPlot data={scatterData}/>
        </div>
    );

}

export default ArtistPopularityChart;