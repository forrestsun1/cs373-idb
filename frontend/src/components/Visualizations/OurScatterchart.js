// Credit: https://gitlab.com/dandom25/electrends/
import React from "react";
import { CartesianGrid, Legend, Scatter, ScatterChart, Tooltip, XAxis, YAxis } from "recharts";
import { Row } from 'reactstrap';

function OurScatterChart(props) {

    console.log("scatterdata", props?.data);
    
    return(
        <Row className="justify-content-center">
        <ScatterChart width={730} height={400}
            margin={{ top: 20, right: 20, bottom: 10, left: 20 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="x" type="number" name="Championship Year" domain={[1930, 2022]} label={{ value: 'Championship Year', position: 'insideBottomRight', offset: -10 }} />
            <YAxis dataKey="y" type="number" name="Total Points" domain={[0, 200]} label={{ value: 'Total Points', angle: -90, position: 'insideLeft' }} />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Legend />
            <Scatter name="Championship Game" data={props.data[0]} fill= "#0015bc" />
           
        </ScatterChart></Row>
    );

}

export default OurScatterChart;