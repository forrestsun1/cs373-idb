// Credit: https://gitlab.com/dandom25/electrends/
import React from "react";
import { Bar, BarChart, CartesianGrid, Tooltip, XAxis, YAxis } from "recharts";

import { Row } from 'reactstrap';

function OurBarChart(props) {

    console.log("bardata", props?.data);
    
    return(
        <Row className="justify-content-center">
        <BarChart width={750} height={300} data={props?.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" height={60} label={{ value: 'States', position: 'center', dy: 10}}/>
            <YAxis dataKey="numTeams" domain={[0, 30]} tickCount={4} label={{ value: 'Number of Teams', position: 'center', angle:-90, dx: -10}}/>
            <Tooltip />
            <Bar dataKey="numTeams" />
        </BarChart></Row>
    );

}

export default OurBarChart;
