// Credit: https://gitlab.com/dandom25/electrends/

import ScatterPlot from "./OurScatterchart";
import React, { useEffect, useState } from "react";

function PointsOverYearsChart() {

  const [scatterData, setScatterData] = useState([]);

  useEffect(() => {
    var api_url = `https://api.marchmadnessmayhem.me/championships`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );
      let body = []
      body = await response.json()
      console.log("BODY[data]")
      console.log((body['data']))
      setScatterData(computeScatterData(body['data']));
    };
    getData();
  }, []);

  const computeScatterData = (gameData) => {
    const array = [];

    gameData.forEach(game => {
      array.push({"x": parseInt(game.id), "y": parseInt(game.score.split("-")[0]) + parseInt(game.score.split("-")[1])});
        });
        array.sort((a, b) => (a.x > b.x) ? 1 : -1);
      
      return [array];
    }

    return(
        <div>
            <h4>Relationship Between Championship Year and the Total Points Scored</h4>
            <ScatterPlot data={scatterData}/>
        </div>
    );

}

export default PointsOverYearsChart;