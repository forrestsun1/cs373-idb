import { Container } from "react-bootstrap";
import NumArtistsChart from "./NumArtistsChart";
import ArtistPopularityChart from "./ArtistPopularityChart";
import SongsInGenresChart from "./SongsInGenresChart";
import TeamsPerStateChart from "./TeamsPerStateChart";
import PointsOverYearsChart from "./PointsOverYearsChart";
import PlayerPositionDistribution from "./PlayerPositionDistribution";

function Visualizations() {
    return (
        <div className="justify-content-center">
            <Container>
                <h1 className="my-3 d-flex justify-content-center">March Madness Mayhem Visualizations</h1>
                    <br />
                    <TeamsPerStateChart/> <br />
                    <PointsOverYearsChart/> <br />
                    <PlayerPositionDistribution/> <br />
        
            </Container>
        
            <Container>
                <h1 className="my-3 d-flex justify-content-center">Low Keys Visualizations</h1>
                    <br />
                    <NumArtistsChart/> <br />
                    <ArtistPopularityChart/> <br />
                    <SongsInGenresChart/> <br />
        
            </Container>
        </div>
    ); 

};




export default Visualizations;
