// Credit: https://gitlab.com/dandom25/electrends/
import PieChart from "./OurPieChart";
import React, { useEffect, useState } from "react";

function PlayerPositionDistribution() {

    const [pieData, setPieData] = useState([]);

  useEffect(() => {
    var api_url = `https://api.marchmadnessmayhem.me/players?page=1`;
    const getData = async () => {
      let response = await fetch (
        api_url,
        { mode: 'cors', }
      );

      let body = []
      body = await response.json()

      console.log("results=" + JSON.stringify(body['data']))
      setPieData(computePieData(body['data']));
    };
    getData();
  }, []);

    const computePieData = (posData) => {
        var data = [{"name": "G", "value": 0},
                    {"name": "F", "value": 0},
                    {"name": "C", "value": 0}];

        const cnts = [0, 0, 0];

        posData.forEach(players => {
            if(players.position.includes("G")){
                data[0].value++;
                ++cnts[0];
            } 
            if(players.position.includes("F")){
                data[1].value++;
                ++cnts[1];
            }
            if(players.position.includes("C")){
                data[2].value++;
                ++cnts[2];
            }
        });
        console.log("data = " + JSON.stringify(data))
        return data;
    }

    return(
        <div>
            <h4>Distribution of Player Positions</h4>
            <PieChart data={pieData}/>
        </div>
    );



}

export default PlayerPositionDistribution;