// Credit: https://gitlab.com/dandom25/electrends/
import React from "react";
import { CartesianGrid, Legend, Scatter, ScatterChart, Tooltip, XAxis, YAxis } from "recharts";
import { Row } from 'reactstrap';

function ProviderScatterChart(props) {

    console.log("scatterdata", props?.data);
    
    return(
        <Row className="justify-content-center">
        <ScatterChart width={730} height={400}
            margin={{ top: 20, right: 20, bottom: 10, left: 20 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="x" type="number" name="Artist Popularity" domain={[0, 5]} label={{ value: 'Artist Popularity', position: 'insideBottomRight', offset: -10 }} />
            <YAxis dataKey="y" type="number" name="Artist Followers" domain={[0, 100]} label={{ value: 'Artist Followers', angle: -90, position: 'insideLeft' }} />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Legend />
            <Scatter name="Artists" data={props.data[0]} fill= "#0015bc" />
           
        </ScatterChart></Row>
    );

}

export default ProviderScatterChart;