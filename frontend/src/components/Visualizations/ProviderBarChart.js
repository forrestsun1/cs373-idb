// Credit: https://gitlab.com/dandom25/electrends/
import React from "react";
import { Bar, BarChart, CartesianGrid, Tooltip, XAxis, YAxis } from "recharts";

import { Row } from 'reactstrap';

function ProviderBarChart(props) {

    console.log("bardata", props?.data);
    
    return(
        <Row className="justify-content-center">
        <BarChart width={750} height={250} data={props?.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name"/>
            <YAxis dataKey="numArtists"/>
            <Tooltip />
            <Bar dataKey="numArtists" />
        </BarChart></Row>
    );

}

export default ProviderBarChart;
