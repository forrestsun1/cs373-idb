
import { Navbar, Nav, Container, Form, FormControl, Button, InputGroup } from "react-bootstrap";
import React from "react";
import { useState } from 'react';

export default function NavBar() {
  const [searchBarInput, setSearchBarInput] = useState("");
  
  const handleSubmit = (event) => {
    event.preventDefault();
    if (searchBarInput.trim().length === 0) {
      alert("Please enter a non-empty search query.");
      return;
    }
    const query = searchBarInput.replace(" ", "%20");
    window.location.href = "/search/" + query;
  };
  
    
  return (
      <Navbar
        collapseOnSelect
        className="Navbar-custom"
        expand="lg"
        variant="dark"
      >
        <Container>
          <Navbar.Brand className="Title-text" href="/" style={{color: 'white'}}>
            March Madness Mayhem
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
            <Nav>
              <Nav.Link href="/about" style={{color: 'white'}} className="nav-link">About</Nav.Link></Nav>
              <Nav.Link href="/games" style={{color: 'white'}} className="nav-link">Games</Nav.Link>
              <Nav.Link href="/teams" style={{color: 'white'}} className="nav-link">Teams</Nav.Link>
              <Nav.Link href="/players" style={{color: 'white'}} className="nav-link">Players</Nav.Link>
              <Nav.Link href="/visualizations" style={{color: 'white'}} className="nav-link">Visualizations</Nav.Link>
            </Nav>
            <Container className="d-flex justify-content-end">
              <Form onSubmit={handleSubmit} inline="true">
                <InputGroup>
                  <FormControl type="search" 
                  style={{ width: "20vw" }}
                  placeholder="Search March Madness Info" 
                  value={searchBarInput}
                  aria-label="Search"
                  onChange={(e) => setSearchBarInput(e.target.value)} />
                  <Button type="submit" style={{backgroundColor: '#5ac2db'}}>Search</Button>
                </InputGroup>
              </Form>
          </Container>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );    
  }