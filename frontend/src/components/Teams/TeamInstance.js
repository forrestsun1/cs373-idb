import axios from 'axios';
import React, { useEffect, useState } from "react";
import { Container, Row, Col, Figure } from 'react-bootstrap';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';

export default function TeamInstance () {
  const params = useParams()
  const [teamData, setTeamData] = useState([]);
  const [confDescription, setConfDescription] = useState("");
  let id = Number(params['id'])

  useEffect(() => {
    const loadTeam = async () => {
      axios.get(`https://api.marchmadnessmayhem.me/teams/${id}`)
        .then(response => {
          setTeamData(response["data"].data);
          console.log(response["data"].data);
          let conf = response["data"].data.conference;
          switch (conf) {
            case "American Athletic":
              setConfDescription("The American Athletic Conference (AAC), also known as the American, is an American collegiate athletic conference, featuring 11 member universities and five affiliate member universities that compete in the National Collegiate Athletic Association's (NCAA) Division I, with its football teams competing in the Football Bowl Subdivision (FBS). Member universities represent a range of private and public universities of various enrollment sizes located primarily in urban metropolitan areas in the Northeastern, Midwestern, and Southern regions of the United States.");
              break;
            case "Atlantic Coast":
              setConfDescription("The Atlantic Coast Conference (ACC) is a collegiate athletic conference located in the Eastern United States. Headquartered in Greensboro, North Carolina, the ACC's fifteen member universities compete in the National Collegiate Athletic Association (NCAA)'s Division I. ACC football teams compete in the NCAA Division I Football Bowl Subdivision. The ACC sponsors competition in twenty-five sports with many of its member institutions held in high regard nationally. Current members of the conference are Boston College, Clemson University, Duke University, Georgia Institute of Technology, Florida State University, North Carolina State University, Syracuse University, the University of Louisville, the University of Miami, the University of North Carolina, the University of Notre Dame, the University of Pittsburgh, the University of Virginia, Virginia Polytechnic Institute and State University, and Wake Forest University.");
              break;
            case "Big 12":
              setConfDescription("The Big 12 Conference is a college athletic conference headquartered in Irving, Texas. It consists of ten full-member universities. It is a member of Division I of the National Collegiate Athletic Association (NCAA) for all sports. Its football teams compete in the Football Bowl Subdivision (FBS; formerly Division I-A), the higher of two levels of NCAA Division I football competition. Its 10 members, in the states of Iowa, Kansas, Oklahoma, Texas and West Virginia, include two private Christian universities and eight public universities. Additionally, the Big 12 has 12 affiliate members — eight for the sport of wrestling, one for women's equestrianism, one for women's gymnastics and two for women's rowing. The Big 12 Conference is a 501(c)(3) nonprofit organization. Brett Yormark became the new commissioner on August 1, 2022.");
              break;
            case "Big Ten":
              setConfDescription("The Big Ten Conference (stylized B1G, formerly the Western Conference and the Big Nine Conference) is the oldest Division I collegiate athletic conference in the United States. Founded as the Intercollegiate Conference of Faculty Representatives in 1896, it predates the founding of its regulating organization, the NCAA. It is based in the Chicago area in Rosemont, Illinois. For many decades the conference consisted of 10 universities. As of 2014, it consists of 14 member institutions and 2 affiliate institutions, with 2 new member institutions scheduled to join in 2024. The conference competes in the NCAA Division I and its football teams compete in the Football Bowl Subdivision (FBS), formerly known as Division I-A, the highest level of NCAA competition in that sport.");
              break;
            case "Conference USA":
              setConfDescription("Conference USA (C-USA or CUSA) is an intercollegiate athletic conference whose current member institutions are located within the Southern United States. The conference participates in the NCAA's Division I in all sports. C-USA's offices are located in Dallas, Texas. C-USA was founded in 1995 by the merger of the Metro Conference and Great Midwest Conference, two Division I conferences that did not sponsor football.");
              break;
            case "Independents":
              setConfDescription("National Collegiate Athletic Association (NCAA) Football Bowl Subdivision independent schools are four-year institutions whose football programs are not part of an NCAA-affiliated conference. This means that FBS independents are not required to schedule each other for competition like conference schools do.");
              break;
            case "Mid-American":
              setConfDescription("The Mid-American Conference (MAC) is a National Collegiate Athletic Association (NCAA) Division I collegiate athletic conference with a membership base in the Great Lakes region that stretches from Western New York to Illinois. Nine of the twelve full member schools are in Ohio and Michigan, with single members located in Illinois, Indiana, and New York. For football, the MAC participates in the NCAA's Football Bowl Subdivision. The MAC is headquartered in the Public Square district in downtown Cleveland, Ohio, and has two members in the nearby Akron area. The conference ranks highest among all ten NCAA Division I FBS conferences for graduation rates.");
              break;
            case "Mountain West":
              setConfDescription("The Mountain West Conference (MW) is one of the collegiate athletic conferences affiliated with the National Collegiate Athletic Association (NCAA) Division I Football Bowl Subdivision (FBS) (formerly I-A). The MW officially began operations on January 4, 1999. Geographically, the MW covers a broad expanse of the Western United States, with member schools located in California, Colorado, Hawaii, Idaho, Nevada, New Mexico, Utah, and Wyoming. Gloria Nevarez took over as Commissioner of the MW on January 1, 2023, following the retirement of founding commissioner Craig Thompson. The charter members of the MW included the United States Air Force Academy, Brigham Young University, Colorado State University, San Diego State University, the University of New Mexico, the University of Nevada, Las Vegas, University of Utah and the University of Wyoming.");
              break;
            case "Pac-12":
              setConfDescription("The Pac-12 Conference is a collegiate athletic conference, that operates in the Western United States, participating in 24 sports at the NCAA Division I level. Its football teams compete in the Football Bowl Subdivision (FBS; formerly Division I-A), the highest level of college football in the nation. The conference's 12 members are located in the states of Arizona, California, Colorado, Oregon, Utah, and Washington. They include each state's flagship public university, four additional public universities, and two private research universities. The modern Pac-12 conference formed after the disbanding of the Pacific Coast Conference (PCC), whose principal members founded the Athletic Association of Western Universities (AAWU) in 1959. The conference previously went by the names Big Five, Big Six, Pacific-8, and Pacific-10. The Pac-12 moniker was adopted in 2011 with the addition of Colorado and Utah.");
              break;
            case "Southeastern":
              setConfDescription("The Southeastern Conference (SEC) is an American college athletic conference whose member institutions are located primarily in the South Central and Southeastern United States. Its fourteen members include the flagship public universities of eleven states, two additional public land-grant universities, and one private research university. The conference is headquartered in Birmingham, Alabama. The SEC participates in the National Collegiate Athletic Association (NCAA) Division I in sports competitions; for football it is part of the Football Bowl Subdivision (FBS), formerly known as Division I-A.");
              break;
            case "Sun Belt":
              setConfDescription("The Sun Belt Conference (SBC) is a collegiate athletic conference that has been affiliated with the NCAA's Division I since 1976. Originally a non-football conference, the Sun Belt began sponsoring football in 2001. Its football teams participate in the Division I Football Bowl Subdivision (FBS). The 14 member institutions of the Sun Belt are distributed across the Southern United States.");
              break;
            case "Atlantic Sun":
              setConfDescription("The ASUN Conference, formerly the Atlantic Sun Conference, is a collegiate athletic conference operating mostly in the Southeastern United States. The league participates at the NCAA Division I level, and began sponsoring football at the Division I FCS level in 2022. Originally established as the Trans America Athletic Conference (TAAC) in 1978, it was renamed as the Atlantic Sun Conference in 2001, and then rebranded as the ASUN Conference in 2016. The conference headquarters are located in Atlanta.");
              break;
            case "Western Athletic":
              setConfDescription("The Western Athletic Conference (WAC) is an NCAA Division I conference. The WAC covers a broad expanse of the western United States with member institutions located in Arizona, California, New Mexico, Utah, Washington, and Texas. Due to most of the conference's football-playing members leaving the WAC for other affiliations, the conference discontinued football as a sponsored sport after the 2012–13 season and left the NCAA's Football Bowl Subdivision (formerly known as Division I-A).");
              break;
            case "Big Sky":
              setConfDescription("The Big Sky Conference (BSC) is a collegiate athletic conference affiliated with the NCAA's Division I with football competing in the Football Championship Subdivision. As of 2023, full member institutions are located in the states of Arizona, California, Colorado, Idaho, Montana, Oregon, Utah, and Washington. Four affiliate members each participate in one sport: two from California are football–only participants and two from the Northeast participate only in men's golf.");
              break;
            case "Big South":
              setConfDescription("The Big South Conference is a collegiate athletic conference affiliated with the NCAA's Division I. Originally a non-football conference, the Big South began sponsoring football in 2002 as part of the Football Championship Subdivision (FCS). The Big South, founded in 1983, is firmly rooted in the South Atlantic region of the United States, with full member institutions located in North Carolina, South Carolina, and Virginia. Associate members are located in Georgia, North Carolina, Pennsylvania, Rhode Island, and South Carolina.");
              break;
            case "Colonial Athletic":
              setConfDescription("The Colonial Athletic Association (CAA) is a collegiate athletic conference affiliated with the NCAA's Division I whose full members are located in East Coast states, from Massachusetts to South Carolina. Most of its members are public universities, and the conference is headquartered in Richmond. The CAA was historically a Southern conference until the addition of four schools in the Northeastern United States (of five that joined from rival conference America East) after the turn of the 21st century, which added geographic balance to the conference. The CAA was founded in 1979 as the ECAC South basketball league.");
              break;
            case "Ivy League":
              setConfDescription("The Ivy League is an American collegiate athletic conference comprising eight private research universities in the Northeastern United States. The term Ivy League is typically used beyond the sports context to refer to the eight schools as a group of elite colleges with connotations of academic excellence, selectivity in admissions, and social elitism. Its members are Brown University, Columbia University, Cornell University, Dartmouth College, Harvard University, Princeton University, University of Pennsylvania, and Yale University. While the term was in use as early as 1933, it became official only after the formation of the athletic conference in 1954.");
              break;
            case "Mid-Eastern":
              setConfDescription("The Mid-Eastern Athletic Conference (MEAC) is a collegiate athletic conference whose full members are historically black colleges and universities (HBCUs) in the Southeastern and the Mid-Atlantic United States. It participates in the National Collegiate Athletic Association's (NCAA) Division I, and in football, in the Football Championship Subdivision (FCS).");
              break;
            case "Missouri Valley":
              setConfDescription("The Missouri Valley Conference (also called MVC or simply \"The Valley\") is the third-oldest collegiate athletic conference in the United States. The conference's members are primarily located in the midwest. The MVC was established in 1907 as the Missouri Valley Intercollegiate Athletic Association or MVIAA, 12 years after the Big Ten, the only Division I conference that is older.");
              break;
            case "Northeast":
              setConfDescription("The Northeast Conference (NEC) is a collegiate athletic conference whose schools are members of the National Collegiate Athletic Association (NCAA). Teams in the NEC compete in Division I for all sports; football competes in the Division I Football Championship Subdivision (FCS). Participating schools are located principally in the Northeastern United States, from which the conference derives its name.");
              break;
            case "Ohio Valley":
              setConfDescription("The Ohio Valley Conference (OVC) is a collegiate athletic conference which operates in the Midwestern and Southeastern United States. It participates in Division I of the NCAA; the conference's football programs compete in the Football Championship Subdivision (FCS; formerly known as Division I-AA), the lower of two levels of Division I football competition. The OVC has 10 members, six of which compete in football in the conference.");
              break;
            case "Patriot League":
              setConfDescription("The Patriot League is a collegiate athletic conference comprising private institutions of higher education and two United States service academies based in the Northeastern United States. Outside the Ivy League, it is among the most selective groups of higher education institutions in the NCAA, and has a very high student-athlete graduation rate for both the NCAA graduation success rate and the federal graduation rate.");
              break;
            case "Southern":
              setConfDescription("The Southern Conference (SoCon) is a collegiate athletic conference affiliated with the National Collegiate Athletic Association (NCAA) Division I. Southern Conference football teams compete in the Football Championship Subdivision (formerly known as Division I-AA). Member institutions are located in the states of Alabama, Georgia, North Carolina, South Carolina, Tennessee, and Virginia. Established in 1921, the Southern Conference ranks as the fifth-oldest major college athletic conference in the United States, and either the third- or fourth-oldest in continuous operation, depending on definitions.");
              break;
            case "Southland":
              setConfDescription("The Southland Conference, abbreviated as SLC, is a collegiate athletic conference which operates in the South Central United States (specifically Texas and Louisiana). It participates in the NCAA's Division I for all sports; for football, it participates in the Division I Football Championship Subdivision (FCS). The Southland sponsors 18 sports, 10 for women and eight for men, and is governed by a presidential Board of Directors and an Advisory Council of athletic and academic administrators. Chris Grant became the Southland's seventh commissioner on April 5, 2022.");
              break;
            case "Southwestern Athleti":
              setConfDescription("The Southwestern Athletic Conference (SWAC) is a collegiate athletic conference headquartered in Birmingham, Alabama, which is made up of historically black colleges and universities (HBCUs) in the Southern United States. It participates in the NCAA's Division I for most sports; in football, it participates in the Football Championship Subdivision (FCS), formerly referred to as Division I-AA. The SWAC is considered the premier HBCU conference and ranks among the elite in the nation in terms of alumni affiliated with professional sports teams, particularly in football. On the gridiron, the conference has been the biggest draw on the Football Championship Subdivision (FCS) level of the NCAA, leading the nation in average home attendance every year except one since FCS has been in existence. In 1994, the SWAC fell just 40,000 fans short of becoming the first non-Football Bowl Subdivision conference to attract one million fans to its home games.");
              break;
            case "America East":
              setConfDescription("The America East Conference is a collegiate athletic conference affiliated with NCAA Division I whose members are located in the Northeastern United States. The America East Conference was founded as the Eastern College Athletic Conference-North, a men's basketball-only athletic conference in 1979. The conference has nine core members including eight public research universities, three of which: the University of Maine, the University of New Hampshire, and the University of Vermont, are the flagship universities of their states. Two non-flagship university centers of the State University of New York, the University at Albany and Binghamton University, are in the conference along with UMass Lowell, NJIT and Bryant University.");
              break;
            case "Big East":
              setConfDescription("The Big East Conference is a collegiate athletic conference that competes in NCAA Division I in ten men's sports and twelve women's sports. Headquartered in New York City, the eleven full-member schools are primarily located in Northeast and Midwest metropolitan areas. The conference was officially recognized as a Division I multi-sport conference on August 1, 2013, and since then conference members have won NCAA national championships in men's basketball, women's cross country, field hockey, men's lacrosse, and men's soccer. Val Ackerman is the commissioner. The conference was formed after the \"Catholic Seven\" members of the original Big East Conference elected to split from the football-playing schools in order to start a new conference focused on basketball. ");
              break;
            case "Atlantic 10":
              setConfDescription("The Atlantic 10 Conference (A-10) is a collegiate athletic conference whose schools compete in the National Collegiate Athletic Association's (NCAA) Division I. The A-10's member schools are located in states mostly on the United States Eastern Seaboard, as well as some in the Midwest: Massachusetts, New York, North Carolina, Pennsylvania, Rhode Island, Virginia, Ohio, Illinois, and Missouri as well as in the District of Columbia. Although some of its members are state-funded, half of its membership is made up of private, Catholic institutions. Despite the name, there are 15 full-time members, and four affiliate members that participate in women's field hockey and men's lacrosse. The current commissioner is Bernadette McGlade, who began her tenure in 2008.");
              break;
            case "Big West":
              setConfDescription("The Big West Conference (BWC) is an American collegiate athletic conference whose member institutions participate in the National Collegiate Athletic Association's Division I. The conference was originally formed on July 1, 1969, as the Pacific Coast Athletic Association (PCAA), and in 1988 was renamed the Big West Conference. The conference stopped sponsoring college football after the 2000 season. Among the conference's 11 member institutions, 10 are located in California (with 9 located in Southern California alone) and one is located in Hawaii. All of the schools are public universities, with the California schools evenly split between the California State University and the University of California systems. In addition, one affiliate member plays two sports in the BWC not sponsored by its home conference.");
              break;
            case "Horizon League":
              setConfDescription("The Horizon League is an 11-school collegiate athletic conference in the National Collegiate Athletic Association (NCAA) Division I, whose members are located in and near the Great Lakes region. The Horizon League founded in 1979 as the Midwestern City Conference. The conference changed its name to Midwestern Collegiate Conference in 1985 and then the Horizon League in 2001. The conference started with a membership of six teams and has fluctuated in size with 24 different schools as members at different times. The League currently has 11 members. Its most recent membership changes occurred on July 1, 2022 with the departure of the University of Illinois Chicago (UIC) to the Missouri Valley Conference. The Horizon League does not sponsor football.");
              break;
            case "West Coast":
              setConfDescription("The West Coast Conference (WCC) — known as the California Basketball Association from 1952 to 1956 and then as the West Coast Athletic Conference until 1989 — is a collegiate athletic conference affiliated with NCAA Division I consisting of ten member schools across the states of California, Oregon, Utah, and Washington. All of the current members are private, faith-based institutions. Seven members are Catholic Church affiliates, with four of these schools being Jesuit institutions. Pepperdine is an affiliate of the Churches of Christ. Brigham Young University is an affiliate of the Church of Jesus Christ of Latter-day Saints. The conference's newest member, the University of the Pacific (which rejoined in 2013 after a 42-year absence), is affiliated with the United Methodist Church, although it has been financially independent of the church since 1969.");
              break;
            case "Summit":
              setConfDescription("The Summit League, or The Summit, is an NCAA Division I intercollegiate athletic conference with its membership mostly located in the Midwestern United States, from Illinois on the east of the Mississippi River to the Dakotas and Nebraska on the West, with additional members in the Western state of Colorado and the Southern state of Oklahoma. Founded as the Association of Mid-Continent Universities in 1982,[1] it rebranded as the Mid-Continent Conference in 1989,[3] then again as the Summit League on June 1, 2007.");
              break;
            case "Metro Atlantic Athle":
              setConfDescription("The Metro Atlantic Athletic Conference (MAAC, /mæk/) is a collegiate athletic conference affiliated with NCAA Division I. Of its current 11 full members, 10 are located in three states of the northeastern United States: Connecticut, New Jersey, and New York. The other member is in Maryland. Members are all relatively small private institutions, a majority Catholic or formerly Catholic, with the only exceptions being two secular institutions: Rider University and Quinnipiac University. The MAAC currently sponsors 25 sports and has 17 associate member institutions.");
              break;
            default:
              setConfDescription("This team has not formally affiliated itself with any basketball conference.");
              break;
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
    loadTeam();
  });

  return (
    <Container style={{justifyContent:'flex'}} className="d-grid">
      <Row style={{marginTop: '25px'}}>
        <Col>
          <h1 className="my-4 d-flex justify-content-start"> {teamData.school} {teamData.name} </h1>
        </Col>
      </Row>
      <Row>
        <Col className="d-flex justify-content-start">
          <Figure>
            <Figure.Image
              src= {teamData.logo}/>
          </Figure>
        </Col>
        <Col className="d-flex justify-content-start">
          <div className="justify-content-left d-flex">
            <iframe
              title="team_stadium_map"
              width="600"
              height="370"
              loading="lazy"
              allowFullScreen
              referrerPolicy="no-referrer-when-downgrade"
              src={teamData.stadiumName ? `https://www.google.com/maps/embed/v1/place?key=AIzaSyBfS2ssxFGS8V0EDjwtkAlF95wHcejdeKE&q="${teamData.stadiumName} in ${teamData.cityLocation}, ${teamData.stateLocation}"` : `https://www.google.com/maps/embed/v1/place?key=AIzaSyBfS2ssxFGS8V0EDjwtkAlF95wHcejdeKE&q="${teamData.school} ${teamData.name}`}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <Col style={{textAlign: 'left'}}>
          <br></br>
          <p><b>Current Wins: </b>{teamData.currentWins ? teamData.currentWins : "???"} </p>
          <p><b>Current Losses: </b>{teamData.currentLosses ? teamData.currentLosses : "???"}</p>
          <p><b>Conference: </b>{teamData.conference ? teamData.conference : "???"}</p>
          <p><b>Stadium Location: </b>{teamData.stadiumName ? `${teamData.stadiumName} in ${teamData.cityLocation}, ${teamData.stateLocation}` : "???"}</p>
          
        </Col>
            <Col style={{textAlign: 'left'}}>
            <div>
              <h4>Learn More About {teamData.school}'s conference:<br></br><i>The {teamData.conference ? teamData.conference : "???"} Conference</i></h4>
              <h6>{confDescription}</h6>
            </div>
            </Col>  
        </Row>
        <Row>
          <Col style={{textAlign: 'left'}}>
            <h4>Check out {teamData.school}'s archive of players:</h4>
            {teamData.players?.length ? 
              <ol>
                {teamData.players.map((player) => (
                  <li key={player.key}>
                    <Link to={`/players/${player.id}`}>
                      {player.name}
                    </Link>
                  </li>
                ))}
              </ol> :
              <h5><i>No players were found for the {teamData.school} {teamData.name}.</i></h5>
            }
          </Col>
          <Col style={{textAlign: 'left'}}>
            <h4>Check out {teamData.school}'s archive of championship wins:</h4>
            {teamData.championships?.length?
              <ol>
                {teamData.championships?.map((game) => 
                  <li key={game.key}>
                    <Link to={`/games/${game.id}`}>{game.id} National Championship Against {game.runnerup}</Link>
                  </li>
                )}
              </ol> : 
              <h5><i>The {teamData.school} {teamData.name} do not have any championship wins.</i></h5>
            }
          </Col>
        </Row>
       </Container>
    )
}