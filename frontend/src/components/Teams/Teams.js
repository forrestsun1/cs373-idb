import axios from 'axios';
import React, { useEffect, useState } from "react";
import TeamCard from "../Cards/TeamCard";
import { Container, Row, Col } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import Loading from "../Loading";
import FilterDropdown from "../FilterDropdown";
import Form from "react-bootstrap/Form";
import RangeSlider from '../RangeSlider';


export default function Teams() {
  const [teamsData, setTeamsData] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const [confChoice, setConfChoice] = useState("All Conferences");
  const [stateChoice, setStateChoice] = useState("All States");
  const [wins, setWins] = useState([0,30]);
  const [losses, setLosses] = useState([0,30]);
  const [sortCriteria, setSortCriteria] = useState(""); 
  const [sortOrder, setSortOrder] = useState(""); 
  const teamsPerPage = 15;
  const pagesVisited = pageNumber * teamsPerPage;

  useEffect(() => {
    const loadTeams = async () => {
      setIsLoading(true);
      axios.get("https://api.marchmadnessmayhem.me/teams")
        .then(response => {
          setTeamsData(response["data"].data)
          setIsLoading(false);
        })
        .catch(error => {
          console.error(error);
          setIsLoading(false);
        });
    }
    loadTeams();
  },[]);

  if (isLoading) {
    return <Loading />;
  }

  const filteredTeams = teamsData.filter((team) => {
    if (confChoice === "All Conferences") {
      return true;
    }
    return team.conference === confChoice;
  }).filter((team) => {
    if (stateChoice === "All States") {
      return true;
    }
    return team.stateLocation === stateChoice;
  }).filter((team) =>
    team.name.toLowerCase().includes(searchQuery.toLowerCase())
  ).filter((team) =>
    (team.currentWins >= wins[0] && team.currentWins <= wins[1]) 
  ).filter((team) => 
    (team.currentLosses >= losses[0] && team.currentLosses <= losses[1])
  ).sort((a, b) => {
    if (sortCriteria === "currentWins") {
      if (sortOrder === "asc") {
        return parseInt(a.currentWins) - parseInt(b.currentWins);
      } else {
        return parseInt(b.currentWins) - parseInt(a.currentWins);
      }
    } else if (sortCriteria === "currentLosses") {
      if (sortOrder === "asc") {
        return parseInt(a.currentLosses) - parseInt(b.currentLosses);
      } else {
        return parseInt(b.currentLosses) - parseInt(a.currentLosses);
      }
    } else if (sortCriteria === "name") {
      if (sortOrder === "asc") {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    } else if (sortCriteria === "school") {
      if (sortOrder === "asc") {
        return a.school.localeCompare(b.school);
      } else {
        return b.school.localeCompare(a.school);
      }
    } else if (sortCriteria === "stadiumName") {
      if (sortOrder === "asc") {
        return a.stadiumName.localeCompare(b.stadiumName);
      } else {
        return b.stadiumName.localeCompare(a.stadiumName);
      }
    }
    return 0;
  });

  const highlightText = (text, query) => {
    if (query === "") {
      return text;
    }
  
    const regex = new RegExp(`(${query})`, "gi");
    const parts = text.split(regex);
  
    return parts.map((part, index) =>
      regex.test(part) ? <mark key={index}>{part}</mark> : part
    );
  };
  

  const displayTeams = filteredTeams
    .slice(pagesVisited, pagesVisited + teamsPerPage)
    .map((data) => {
      return (
        <div className="col-sm-4">
          <TeamCard
              id = {data.id}
              name = {highlightText(data.name, searchQuery)}
              school = {highlightText(data.school, searchQuery)}
              currentWins = {data.currentWins || "???"}
              currentLosses = {data.currentLosses || "???"}
              conference = {data.conference || "???"}
              stadiumName = {data.stadiumName || "???"}
              cityLocation = {data.cityLocation || "???"}
              stadiumLocation = {data.stadiumLocation || "???"}
              stateLocation = {data.stateLocation || "???"} 
              logo = {data.logo}
          />
        </div>
      )
  });

  const pageCount = Math.ceil(filteredTeams.length / teamsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const handleWinsFilter = (value) => {
    setWins(value);
  }

  const handleLossesFilter = (value) => {
    setLosses(value);
  }

  const handleConfClick = (value) => {
    setConfChoice(value);
    setSearchQuery("");
    setPageNumber(0);
  };
  
  const handleStateClick = (value) => {
    setStateChoice(value);
    setSearchQuery("");
    setPageNumber(0);
  };

  return (
    <Container>
      <h1 className="my-4 d-flex justify-content-center">All NCAA Teams</h1>
      <Row className='mt-3'>
        <Col className='sm-12 mb-3'>
          <input
            type="text"
            placeholder="Search for a team..."
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </Col>
        <Col>
          <FilterDropdown
            title="Conference"
            items={["All Conferences", "America East", "American Athletic", "Atlantic 10", "Atlantic Coast", "Atlantic Sun", "Big 12", "Big East", "Big Sky", "Big South", "Big Ten", "Big West", "Colonial Athletic", "Conference USA", "Horizon League", "Independents", "Ivy League", "Metro Atlantic Athle", "Mid-American", "Mid-Eastern", "Missouri Valley", "Mountain West", "Northeast", "Ohio Valley", "Patriot League", "Southeastern", "Southern", "Southland", "Southwestern Athleti", "Summit", "Sun Belt", "West Coast", "Western Athletic", null]}
            scroll={true}
            onChange={handleConfClick}
            filteredTeams={filteredTeams}
            searchQuery={searchQuery}
          />
        </Col>
        <Col>
          <FilterDropdown
            title="U.S. State"
            items={["All States", "AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]}
            scroll={true}
            onChange={handleStateClick}
            filteredTeams={filteredTeams}
            searchQuery={searchQuery}
          />
        </Col>
        <Col>
          <Form.Label>Wins</Form.Label>
            <RangeSlider
              min={0}
              max={30}
              onChange={handleWinsFilter}
              discrete
            />
        </Col>
        <Col>
          <Form.Label>Losses</Form.Label>
              <RangeSlider
                min={0}
                max={30}
                onChange={handleLossesFilter}
                discrete
              />
        </Col>
        <Col className='sm-12 mb-3'>  
            <select value={sortCriteria} onChange={(e) => setSortCriteria(e.target.value)}>
            <option value="" disabled selected hidden>Sort Teams...</option>
              <option value="currentWins">Current Wins</option>
              <option value="currentLosses">Current Losses</option>
              <option value="name">Mascot Name</option>
              <option value="school">School Name</option>
              <option value="stadiumName">Stadium Name</option>
            </select>
            <select value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
              <option value="" disabled selected hidden>Sort by...</option>
              <option value="asc">Ascending</option>
              <option value="desc">Descending</option>
            </select>
          </Col>
      </Row>
    <Row>
      {displayTeams}
    </Row>
    <p style={{textAlign: "center"}}>Showing {pagesVisited + 1}-{Math.min(filteredTeams.length ,pagesVisited + teamsPerPage)} out of {filteredTeams.length} teams <br></br> <span style={{color:"gray", textAlign: "center"}}>Page {pageNumber + 1} of {pageCount}</span></p>
    <ReactPaginate
      previousLabel={"<"}
      nextLabel={">"}
      pageCount={pageCount}
      onPageChange={changePage}
      containerClassName={"pagination justify-content-center"}
      previousLinkClassName={"page-link"}
      nextLinkClassName={"page-link"}
      disabledClassName={"page-item disabled"}
      activeClassName={"page-item active"}
      pageClassName={"page-item"}
      pageLinkClassName={"page-link"}
    />
    </Container>
  );
}