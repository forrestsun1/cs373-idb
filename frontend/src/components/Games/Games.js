import axios from 'axios';
import React, { useEffect, useState } from "react";
import GameCard from "../Cards/GameCard";
import march_madness_court from "../../assets/march_madness_court.jpg";
import { Container, Row, Col } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import Loading from "../Loading";
import FilterDropdown from "../FilterDropdown";

export default function Games() {
  const [gamesData, setGamesData] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [isLoading, setIsLoading] = useState(true); // add state variable
  const [searchCoach, setSearchCoach] = useState("");
  const [startingYearChoice, setStartingYear] = useState("1939");
  const [endingYearChoice, setEndingYear] = useState("2022");
  const [winnerSearch, setWinnerSearch] = useState("");
  const [runnerupSearch, setRunnerupSearch] = useState("");
  const [locationChoice, setLocationChoice] = useState("All Locations");
  const [sortCriteria, setSortCriteria] = useState(""); 
  const [sortOrder, setSortOrder] = useState("");
  const gamesPerPage = 12;
  const pagesVisited = pageNumber * gamesPerPage;

  useEffect(() => {
    const loadGames = async () => {
      setIsLoading(true); // set isLoading to true before making the API call
      axios.get("https://api.marchmadnessmayhem.me/championships")
        .then(response => {
          setGamesData(response["data"].data)
          console.log(response["data"].data)
          setIsLoading(false); // set isLoading to false after data is loaded
        })
        .catch(error => {
          console.error(error);
          setIsLoading(false); // set isLoading to false if there's an error
        });
    }
    loadGames();
  },[]);

  // display loading component if isLoading is true
  if (isLoading) {
    return <Loading />;
  }
  
  const filteredGames = gamesData
    .filter((game) => { 
      if (locationChoice === "All Locations") {
        return true;
      }
      return game.location === locationChoice;
    }).filter((game) =>
      game.coach.toLowerCase().includes(searchCoach.toLowerCase())
    ).filter((game) => 
      game.champion.toLowerCase().includes(winnerSearch.toLowerCase())
    ).filter((game) =>
      game.runnerup.toLowerCase().includes(runnerupSearch.toLowerCase())
    ).filter((game) => {
      if (startingYearChoice <= 0 && endingYearChoice <= 0) {
        return true;
      } else if (startingYearChoice <= 0) {
        return (parseInt(game.id) <= parseInt(endingYearChoice))
      } else if (endingYearChoice <= 0) {
        return (parseInt(game.id) >= parseInt(startingYearChoice))
      }
      return parseInt(game.id) >= parseInt(startingYearChoice) && parseInt(game.id) <= parseInt(endingYearChoice);
    }).sort((a, b) => {
      if (sortCriteria === "year") {
        if (sortOrder === "asc") {
          return parseInt(a.id) - parseInt(b.id);
        } else {
          return parseInt(b.id) - parseInt(a.id);
        }
      } else if (sortCriteria === "champion") {
        if (sortOrder === "asc") {
          return a.champion.localeCompare(b.champion);
        } else {
          return b.champion.localeCompare(a.champion);
        }
      } else if (sortCriteria === "runnerup") {
        if (sortOrder === "asc") {
          return a.runnerup.localeCompare(b.runnerup);
        } else {
          return b.runnerup.localeCompare(a.runnerup);
        }
      } else if (sortCriteria === "location") {
        if (sortOrder === "asc") {
          return a.location.localeCompare(b.location);
        } else {
          return b.location.localeCompare(a.location);
        }
      }
      return 0;
    });

    const highlightText = (text, query) => {
      if (query === "") {
        return text;
      }
    
      const regex = new RegExp(`(${query})`, "gi");
      const parts = text.split(regex);
    
      return parts.map((part, index) =>
        regex.test(part) ? <mark key={index}>{part}</mark> : part
      );
    };

    const displayGames = filteredGames
      .slice(pagesVisited, pagesVisited + gamesPerPage)
      .map((data) => {
        return (
          <div className="col-sm-4">
            <GameCard
                id = {data.id}
                champion = {highlightText(data.champion, winnerSearch)}
                runnerup = {highlightText(data.runnerup, runnerupSearch)}
                coach = {highlightText(data.coach, searchCoach)}
                score = {data.score}
                location = {data.location}
                replayUrl = {data.replayUrl}
                img = {march_madness_court}
            />
          </div>
        )
      });

    const pageCount = Math.ceil(filteredGames.length / gamesPerPage);

    const changePage = ({ selected }) => {
      setPageNumber(selected);
    };

    const handleLocationClick = (value) => {
      setLocationChoice(value);
      setSearchCoach("");
      setStartingYear("1939");
      setEndingYear("2022");
      setWinnerSearch("");
      setRunnerupSearch("");
      setPageNumber(0);
    };

    return (
      <Container>
        <h1 className="my-4 d-flex justify-content-center">All NCAA Championship Games</h1>
        <Row className='mt-3'>
          <Col className='sm-12 mb-3'>  
            <input
              type="text"
              placeholder="Search for a coach..."
              value={searchCoach}
              onChange={(e) => setSearchCoach(e.target.value)}
            />
          </Col>
          <Col className='sm-12 mb-3'>  
            <input
              type="text"
              placeholder="Starting Year..."
              value={startingYearChoice}
              onChange={(e) => setStartingYear(e.target.value)}
            />
            <input
              type="text"
              placeholder="Ending Year..."
              value={endingYearChoice}
              onChange={(e) => setEndingYear(e.target.value)}
            />
          </Col>
          <Col className='sm-12 mb-3'>  
            <input
              type="text"
              placeholder="Search Winner..."
              value={winnerSearch}
              onChange={(e) => setWinnerSearch(e.target.value)}
            />
            <input
              type="text"
              placeholder="Search Runner Up..."
              value={runnerupSearch}
              onChange={(e) => setRunnerupSearch(e.target.value)}
            />
          </Col>
          <Col classname='sm-12 mb-3'>
            <FilterDropdown
              title="Location"
              items={["All Locations", "Albuquerque, N.M.", "Arlington, Texas", "Atlanta, Ga.", "College Park, Md.", "Dallas, Texas", "Daly City, Calif.", "Denver, Colo.", "Detroit, Mich.", "East Rutherford, N.J.", "Evanston, Ill.", "Greensboro, N.C.", "Houston, Texas", "Indianapolis, Ind.", "Kansas City, Mo.", "Lexington, Ky,", "Los Angeles, Calif.", "Louisville, Ky.", "Minneapolis, Minn.", "New Orleans, La.", "New York, N.Y.", "Philadelphia, Pa.", "Phoenix, Ariz.", "Portland, Ore.", "Salt Lake City, Utah", "San Antonio, Texas", "San Antonio, Tex.", "San Diego, Calif.", "Seattle, Wash.", "St. Louis, Mo.", "St. Petersburg, Fla."]}
              scroll={true}
              onChange={handleLocationClick}
              filteredGames={filteredGames}
            />
          </Col>
          <Col className='sm-12 mb-3'>  
            <select value={sortCriteria} onChange={(e) => setSortCriteria(e.target.value)}>
            <option value="" disabled selected hidden>Sort Games...</option>
              <option value="year">Year</option>
              <option value="champion">Champion</option>
              <option value="runnerup">Runner Up</option>
              <option value="location">Location</option>
            </select>
            <select value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
              <option value="" disabled selected hidden>Sort by...</option>
              <option value="asc">Ascending</option>
              <option value="desc">Descending</option>
            </select>
          </Col>
        </Row>
        <Row>
          {displayGames}
        </Row>
        <p style={{textAlign: "center"}}>Showing {pagesVisited + 1}-{Math.min(filteredGames.length, pagesVisited + gamesPerPage)} games out of {filteredGames.length} games <br></br> <span style={{color:"gray", textAlign: "center"}}>Page {pageNumber + 1} of {pageCount}</span></p>
        <ReactPaginate
          previousLabel={"<"}
          nextLabel={">"}
          pageCount={pageCount}
          onPageChange={changePage}
          containerClassName={"pagination justify-content-center"}
          previousLinkClassName={"page-link"}
          nextLinkClassName={"page-link"}
          disabledClassName={"page-item disabled"}
          activeClassName={"page-item active"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
        />
      </Container>
    );
  }
