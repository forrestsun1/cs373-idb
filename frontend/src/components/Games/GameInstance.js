import axios from 'axios';
import React, { useEffect, useState } from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom'


export default function GameInstance(){
    const params = useParams()
    const [gameData, setGameData] = useState([]);
    let id = Number(params['id'])

    useEffect(() => {
        const loadGame = async () => {
          axios.get(`https://api.marchmadnessmayhem.me/championships/${id}`)
            .then(response => {
              setGameData(response["data"].data)
              console.log(response["data"].data)
            })
            .catch(error => {
              console.error(error);
            });
        }
        loadGame();
      });
    
    return (
       <Container style={{justifyContent:'flex'}} className="d-grid">
            <Row style={{marginTop: '25px'}}>
                <Col>
                    <h1 className="my-4 d-flex justify-content-start"> {gameData.id}: {gameData.champion} vs. {gameData.runnerup} </h1>
                </Col>
            </Row>
            <Row>
                <Col className="d-flex justify-content-start">
                    <iframe
                        title="player_highlights"
                        width="600"
                        height="370"
                        loading="lazy"
                        allowFullScreen
                        referrerPolicy="no-referrer-when-downgrade"
                        src={gameData.replayUrl ? `https://www.youtube.com/embed/${gameData.replayUrl.substring(32, 43)}` : `https://www.youtube.com/embed/Fuu6IMxywTM`}
                    />
                </Col>
                <Col className="d-flex justify-content-start">
                    <div className="justify-content-left d-flex">
                        <iframe
                        title="team_stadium_map"
                        width="600"
                        height="370"
                        loading="lazy"
                        allowFullScreen
                        referrerPolicy="no-referrer-when-downgrade"
                        src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBfS2ssxFGS8V0EDjwtkAlF95wHcejdeKE&q="${gameData.location}"`}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col style={{textAlign: 'left'}}>
                    <br></br>
                    <p><b>Winner: </b>{gameData.champion} </p>
                    <p><b>{gameData.id} {gameData.champion} Coach: </b>{gameData.coach} </p>
                    <p><b>Runner-Up: </b>{gameData.runnerup} </p>
                    <p><b>Final score: </b>{gameData.score}</p>
                    <p><b>Game Location: </b>{gameData.location} </p>
                </Col>
            </Row>
            <Row>
            <Col style={{textAlign: 'left'}}>
                <h4>Check out {gameData.champion}'s {gameData.id} Game Roster:</h4>
                <ol>
                    {gameData.players?.map((player) => <li key={player.key}><Link to={`/players/${player.id}`}>{player.name}</Link></li>)}
                </ol>
            </Col>
            <Col style={{textAlign: 'left'}}>
                <br></br>
                <Link to={`/teams/${gameData.championID}`}><Button variant="success" size="lg">Learn More About {gameData.champion}</Button></Link>
                <br></br>
            </Col>
        </Row>
       </Container>
    )
}
