import { Table } from "react-bootstrap"

const RepoStats = ({ numCommits, numIssues }) => {
    return (
        <div className="justify-content-start">
            <h2 className="my-3 d-flex justify-content-start">GitLab Repository Stats</h2>
            <Table className="m-3 justify-content-start" bordered striped responsive="-xl">
                <tbody>
                    <tr>
                        <td>Commits</td>
                        <td>Issues</td>
                        <td>Tests</td>
                    </tr>
                    <tr>
                        <td>{numCommits}</td>
                        <td>{numIssues}</td>
                        <td>37</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    ); 
};

export default RepoStats;
