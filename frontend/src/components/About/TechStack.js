import { Container, Row, Card, Button } from "react-bootstrap";
import gitlab from "../../assets/gitlab.png"
import awsamplify from "../../assets/awsamplify.png"
import reactbootstrap from "../../assets/react_bootstrap.png"
import bootstrap from "../../assets/bootstrap.png"
import namecheap from "../../assets/namecheap.png"
import nodejs from "../../assets/nodejs.png"
import postman from "../../assets/postman.png"
import react from "../../assets/react.png"
import flask from "../../assets/flask.png"


const TechStack = () => {
    return (
        <div>
            <h2 className="className=my-3 d-flex justify-content-start">Tools & Tech Stack</h2>
            <Container className="my-4">
                <Row sm>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={gitlab} variant="top" />
                        <Card.Body>
                            <Card.Title>Gitlab</Card.Title>
                            <Card.Text>
                                Repository hosting used for version control.
                            </Card.Text>
                            <Button className="mb-3"
                                href="https://gitlab.com/forrestsun1/cs373-idb/"
                                size="sm"
                            >
                                MMM GitLab Repository
                            </Button>
                            <Button className="btn btn-info"
                                href="https://docs.gitlab.com/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={postman} variant="top" />
                        <Card.Body>
                            <Card.Title>Postman</Card.Title>
                            <Card.Text>
                                REST API design and testing tool.
                            </Card.Text>
                            <Button className="mb-3"
                                href="https://documenter.getpostman.com/view/25819027/2s93CEvbNp"
                                size="sm"
                            >
                                MMM API Documentation
                            </Button>
                            <Button className="btn btn-info"
                                href="https://www.postman.com/product/api-repository/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={react} variant="top" />
                        <Card.Body>
                            <Card.Title>React</Card.Title>
                            <Card.Text>
                                Javascript frontend framework.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://reactjs.org"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={reactbootstrap} variant="top" />
                        <Card.Body>
                            <Card.Title>React Bootstrap</Card.Title>
                            <Card.Text>
                                Javascript library for styled components in
                                React.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://react-bootstrap.github.io/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={bootstrap} variant="top" />
                        <Card.Body>
                            <Card.Title>Bootstrap</Card.Title>
                            <Card.Text>
                                CSS Framework for styled components in
                                web applications.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://getbootstrap.com/docs/5.3/getting-started/introduction/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                </Row>
                <Row className="my-4">
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={namecheap} variant="top" />
                        <Card.Body>
                            <Card.Title>Namecheap</Card.Title>
                            <Card.Text>
                                ICANN-accredited web domain name registrar.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://www.namecheap.com/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={flask} variant="top" />
                        <Card.Body>
                            <Card.Title>Flask</Card.Title>
                            <Card.Text>
                                Python backend framework for REST API
                                implementation.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://flask.palletsprojects.com/en/2.2.x/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={nodejs} variant="top" />
                        <Card.Body>
                            <Card.Title>Node.js</Card.Title>
                            <Card.Text>
                                An asynchronous event-driven JavaScript runtime
                                environment.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://nodejs.org/en/docs/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                    <Card style={{ width: "13rem" }} className="mx-auto">
                        <Card.Img src={awsamplify} variant="top" />
                        <Card.Body>
                            <Card.Title>AWS Amplify</Card.Title>
                            <Card.Text>
                                Cloud Platform for backend and frontend
                                deployment.
                            </Card.Text>
                            <Button className="btn btn-info"
                                href="https://nodejs.org/en/docs/"
                                size="sm"
                            >
                                Learn More
                            </Button>
                        </Card.Body>
                    </Card>
                </Row>
                <Row className="my-4">
                    
                </Row>
                <Row className="my-4">
                    
                </Row>
            </Container>
        </div>
    );
};

export default TechStack;
