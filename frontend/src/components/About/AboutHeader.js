import styles from "./About.module.css";

export default function AboutHeader() {
    return (
        <div>
            <h1 className="my-4 d-flex justify-content-start">About MMM</h1>
            <div className={styles.aboutHeader}>
                <h3 >
                    March Madness Mayhem allows you to access information about  
                    past NCAA Men's Basketball Tournaments.
                </h3>
                <p>
                    Our informational website displays the relationship between the teams,
                    the players, and the championship games throughout past March Madness tournaments. 
                    Throughout our pages listing the championship games since 1939, you can learn 
                    which teams have won the most and least championships. You can also use our Teams 
                    page to explore different teams in the NCAA Division I and their various stats.
                    For players you’re interested in, you can peruse the Players page to learn more
                    about different players, their backgrounds, statistics, and much more.
                </p>
            </div>
        </div>
    );
};