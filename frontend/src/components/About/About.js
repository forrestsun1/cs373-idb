import { Container } from "react-bootstrap";
import AboutHeader from "./AboutHeader";
import Developers from "./Developers";
import Loading from "../Loading"
import RepoStats from "./RepoStats"
import Sources from "./Sources"
import TechStack from "./TechStack"
import useDevStats from "../../hooks/devStats.js";

export default function About() {
    const { devs, numCommits, numIssues } = useDevStats();
    const allDevs = devs?.filter((dev) => dev.commits > 0);
    return (
        <Container>
            <AboutHeader />
            <hr className="mt-3" />
            {!devs ? (  
                <Loading />
            ) : (
                <Developers devs={allDevs} />
            )}
            <hr className="mt-3" />
            <RepoStats numCommits={numCommits} numIssues={numIssues} />
            <hr className="mt-3" />
            <Sources />
            <hr className="mt-3" />
            <TechStack />
        </Container>
    );
};
