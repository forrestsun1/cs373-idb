import { Card, Badge, Container, Row, Col } from "react-bootstrap";
import gerardo from "../../assets/Gerardo.png";
import anya from "../../assets/Anya.png";
import forrest from "../../assets/Forrest.png";
import irfan from "../../assets/Irfan.png";
import kenneth from "../../assets/Kenneth.png";

const Devs = ({ devs }) => {
    const images = [forrest, anya, kenneth, gerardo, irfan];
    const bios = [
        "I am a junior at UT Austin majoring in Computer Science and getting a certificate in Digital Arts & Media. In my free time, I enjoy playing and watching sports, especially ultimate frisbee and rock climbing!",
        "I am a junior at UT Austin majoring in Computer Science and minoring in Business. In my free time, I enjoy playing and watching sports, especially basketball and spikeball!",
        "I am a junior at UT Austin majoring in Computer Science. In my free time, I enjoy watching sports and playing video games. I also enjoy playing basketball and golf with friends.",
        "I am a junior at UT Austin majoring in Computer Science and Mathematics. I am also getting a minor in business and a certificate in Applied Statistical Modeling. In my free time, I enjoy playing and watching sports, especially basketball and volleyball!",
        "I am a sophomore at UT Austin majoring in Computer Science. In my free time, I enjoy playing and watching sports, especially football and boxing!",
    ];
    const tests = [0, 25, 6, 0, 6];
    const roles = ["Front-End & AWS", "Full Stack", "Back-End", "Front-End", "Back-End"]; 

    return (
        <div>
            <h2 className="my-4 d-flex justify-content-start">Developers</h2>
            <Container>
                <Row>
                    {devs.map((dev, i) => (
                        <Col key={i} sm='auto' className='mb-2 mx-0'>
                            <Card style={{width: '14rem'}}  className='h-100'>
                                <Card.Img
                                    variant="top"
                                    src={images[i]}
                                ></Card.Img>
                                <Card.Title className="pt-3">
                                    {dev.name}
                                </Card.Title>
                                <Card.Title className="pb-3">
                                    Role: {roles[i]}
                                </Card.Title>
                                <Card.Text className="mx-4 mb-4">
                                    {bios[i]}
                                </Card.Text>
                                <Card.Text className="px-3 pb-2 mx-auto">
                                    Commits:{" "}
                                    <Badge
                                        className="p-2 mb-2"
                                        variant="primary"
                                    >
                                        {dev.commits}
                                    </Badge>
                                    <br />
                                    Issues:{" "}
                                    <Badge
                                        className="p-2 mb-2"
                                        variant="primary"
                                    >
                                        {dev.issues}
                                    </Badge>
                                    <br />
                                    Tests:{" "}
                                    <Badge
                                        className="p-2 mb-2"
                                        variant="primary"
                                    >
                                        {tests[i]}
                                    </Badge>
                                </Card.Text>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
};

export default Devs;
