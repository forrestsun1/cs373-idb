import { Card, Container, Row } from "react-bootstrap";
import sportsdataio from "../../assets/sportsdataio.png";
import sportradar from "../../assets/sportradar-logo.png";
import gitlab from "../../assets/gitlab.png"
import rundown from "../../assets/therundown.png"
import mediawiki from "../../assets/mediawiki.png"

const Sources = () => {
    return (
        <div>
            <h2 className="my-3 d-flex justify-content-start">Data Sources and APIs</h2>
            <h6 className="my-3 d-flex justify-content-start"><i>Click on the API's card to learn more.</i></h6>
            <Container className="mt-4">
                <Row sm>
                    <Card
                        as="a"
                        href="https://sportsdata.io/developers/api-documentation/ncaa-basketball"
                        style={{
                            width: "13rem",
                        }}
                        className="mx-auto text-decoration-none text-dark"
                    >
                        <Card.Img src={sportsdataio} variant="top" />
                        <Card.Body>
                            <Card.Title>SportsData.io API</Card.Title>
                            <Card.Text>
                                Used to scrape information about college basketball games.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card
                        as="a"
                        href="https://developer.sportradar.com/docs/read/basketball/NCAA_Mens_Basketball_v7"
                        style={{
                            width: "13rem",
                        }}
                        className="mx-auto text-decoration-none text-dark"
                    >
                        <Card.Img src={sportradar} variant="top" />
                        <Card.Body>
                            <Card.Title>SportRadar API</Card.Title>
                            <Card.Text>
                            Used to scrape information about male college basketball players.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card
                        as="a"
                        href="https://rapidapi.com/therundown/api/therundown"
                        style={{
                            width: "13rem",
                        }}
                        className="mx-auto text-decoration-none text-dark"
                    >
                        <Card.Img src={rundown} variant="top" />
                        <Card.Body>
                            <Card.Title>TheRundown API</Card.Title>
                            <Card.Text>
                            Used to scrape information about NCAA Men's Basketball Championship Games.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card
                        as="a"
                        href="https://www.mediawiki.org/wiki/API:Main_page"
                        style={{
                            width: "13rem",
                        }}
                        className="mx-auto text-decoration-none text-dark"
                    >
                        <Card.Img src={mediawiki} variant="top" />
                        <Card.Body>
                            <Card.Title>MediaWiki API</Card.Title>
                            <Card.Text>
                            Used to scrape pictures for NCAA Men's Basketball Players.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card
                        as="a"
                        href="https://docs.gitlab.com/ee/api/"
                        style={{
                            width: "13rem",
                        }}
                        className="mx-auto text-decoration-none text-dark"
                    >
                        <Card.Img src={gitlab} variant="top" />
                        <Card.Body>
                            <Card.Title>GitLab API</Card.Title>
                            <Card.Text>
                            Used to scrape information about our team's progress in terms of commits and issues.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Row>
            </Container>
        </div>
    );
};

export default Sources;
