import React from 'react';
import { Card, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function GameCard (props) {
    const id = props.id
    const name = props.name
    const position = props.position
    const personalFouls = props.personalFouls
    const points = props.points
    const rebounds = props.rebounds
    const steals = props.steals
    const turnovers = props.turnovers
    const image = props.image

    return (
        <Container>
            <Card className="mt-5" id={id} style={{ width: '25rem' }}>
                <Card.Img variant="top" src={image} height="400"/>
                <Card.Body>
                    <Card.Title><b>{name}</b></Card.Title>
                    <Card.Text> 
                        <b>Position: </b>{position}<br></br>
                        <b>Number of Personal Fouls: </b>{personalFouls} <br></br>
                        <b>Number of Turnovers: </b> {turnovers} <br></br>
                        <b>Number of Points: </b>{points} <br></br>
                        <b>Number of Rebounds: </b>{rebounds} <br></br>
                        <b>Number of Steals: </b> {steals} 
                    </Card.Text>
                    <Link to={`/players/${id}`}><Button a href={`/players/${id}`} variant="primary" target="_blank"><b>Player Info & Stats</b></Button></Link>
                </Card.Body>
            </Card>
        </Container>
    );
};