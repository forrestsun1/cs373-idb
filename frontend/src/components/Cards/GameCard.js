import React from 'react';
import { Card, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function GameCard (props) {
    const id = props.id
    const champion = props.champion
    const runnerup = props.runnerup
    const coach = props.coach
    const score = props.score
    const location = props.location
    const img = props.img

    return (
        <Container>
            <Card className="mt-5" id={id} style={{ width: '25rem' }}>
                <Card.Img variant="top" src={img} style={{objectFit: 'cover', height: '30vh'}}/>
                <Card.Body>
                    <Card.Title><b>{id} National Championship Game</b></Card.Title>
                    <Card.Text> 
                        <b>Winner: </b>{champion}<br></br>
                        <b>{id} {champion} Coach: </b>{coach}<br></br>
                        <b>Runner-Up: </b>{runnerup} <br></br>
                        <b>Final score: </b>{score} <br></br>
                        <b>Location: </b>{location} <br></br>
                    </Card.Text>
                    <Link to={`/games/${id}`}><Button a href={`/games/${id}`} variant="primary" target="_blank"><b>Game Details</b></Button></Link>
                </Card.Body>
            </Card>
        </Container>
    );
};
