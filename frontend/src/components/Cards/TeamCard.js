import React from 'react';
import { Card, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function TeamCard (props) {
    const id = props.id
    const school = props.school
    const name = props.name
    const currentWins = props.currentWins
    const currentLosses = props.currentLosses
    const conference = props.conference
    const stadiumName = props.stadiumName
    const cityLocation = props.cityLocation
    const stateLocation = props.stateLocation
    const logo = props.logo

    return (
        <Container>
            <Card className="mt-5" id={id} style={{ width: '25rem' }}>
                <Card.Img variant="top" src={logo}/>
                <Card.Body>
                    <Card.Title><b>{school} {name}</b></Card.Title>
                    <Card.Text>
                        <b>Current Wins: </b>{currentWins}<br></br>
                        <b>Current Losses: </b>{currentLosses} <br></br>
                        <b>Conference: </b>{conference} <br></br>
                        <b>Stadium Location: </b>{stadiumName} in {cityLocation}, {stateLocation} <br></br>
                    </Card.Text>
                    <Link to={`/teams/${id}`}><Button a href={`/teams/${id}`} variant="primary" target="_blank"><b>Team Info & Stats</b></Button></Link>
                </Card.Body>
            </Card>
        </Container>
    );
};